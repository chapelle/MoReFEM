///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Feb 2016 14:06:43 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_

# include <memory>
# include <vector>

# include "Parameters/ParameterType.hpp"

# include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"

# include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS
        {


            /*!
             * \brief Init a \a InitialCondition from the content of the input parameter file.
             *
             * \tparam LuaFieldT Lua field considered in the input file.
             *
             * \param[in] geometric_mesh_region Mesh upon which the InitialCondition is applied. Initial condition
             * might be requested at each of each \a Coords.
             * \param[in] nature One of the possible nature specified in the comment of the input parameter. Currently
             * only 'constant' and 'lua_function'.
             * \param[in] scalar_value Value to apply if \a nature is 'constant'; irrelevant otherwise.
             * \copydoc doxygen_hide_input_parameter_data_arg
             *
             * \return The \a InitialCondition object properly initialized.
             */
            template
            <
                class LuaFieldT,
                class InputParameterDataT
            >
            InitialCondition<ParameterNS::Type::scalar>::unique_ptr
            InitScalarInitialCondition(const GeometricMeshRegion& geometric_mesh_region,
                                       const InputParameterDataT& input_parameter_data,
                                       const std::string& nature,
                                       double scalar_value);



        } // namespace FormulationSolverNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_INIT_SCALAR_INITIAL_CONDITION_HPP_
