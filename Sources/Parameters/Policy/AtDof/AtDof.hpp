///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Oct 2015 11:57:13 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HPP_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"

# include "Core/LinearAlgebra/GlobalVector.hpp"

# include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp" // \todo #704 Should probably disappear shortly!

# include "FiniteElement/Unknown/Unknown.hpp"
# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

# ifndef NDEBUG
#  include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
# endif // NDEBUG

# include "Parameters/ParameterType.hpp"
# include "Parameters/Policy/AtDof/Internal/AtDof.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricMeshRegion;
    class GeometricElt;

    template<ParameterNS::Type TypeT>
    class FiberList;


    namespace ParameterNS
    {


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT,
            unsigned int NfeltSpaceT
        >
        struct FromParameterAtDof;


    } // namespace ParameterNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \class doxygen_hide_at_dof_policy_tparam
             *
             * \tparam TypeT \a ParameterNS::Type might be scalar or vectorial.
             * \tparam NfeltSpace If 1, only one \a FEltSpace is expected; of any dimension.
             * If 2, two are expected: one that deals with \a LocalFEltSpace the same dimension of the mesh, the other
             * for this dimension minus 1. 3 is possible as well, going up to dimension minus 2.
             */


            /*!
             * \class doxygen_hide_param_at_dof_and_unknown
             *
             * \attention The purpose of to get here the dofs related to a given \a Unknown. The \a GlobalVector from
             * which data are extracted might actually include more, but a single \a ParameterAtDof can provide
             * values for only one of them. If you actually needs both, just define two \a ParameterAtDof, one for each
             * \a Unknown.
             */


            /*!
             * \brief Parameter policy when the parameter is expressed at dofs.
             *
             * \copydetails doxygen_hide_at_dof_policy_tparam
             *
             * \copydetails doxygen_hide_param_at_dof_and_unknown
             *
             * \attention This policy can't be used directly within ParameterInstance: ParameterInstance expects
             * only one template argument, whereas here two are displayed. You should look at \a ParameterAtDof
             * struct to see how to work around this issue (and you shouldn't anyway have to use current policy directly:
             * that's the whole point of \a ParameterAtDof...).
             */
            template
            <
                ParameterNS::Type TypeT,
                unsigned int NfeltSpaceT = 1
            >
            class AtDof
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = AtDof<TypeT, NfeltSpaceT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

            private:

                //! Alias to traits class related to TypeT.
                using traits = Traits<TypeT>;

            public:

                static_assert(TypeT != ParameterNS::Type::matrix,
                              "Irrelevant for this type of parameter.");


                //! Returns numbers of \a FEltSpace in the \a Parameter.
                static constexpr unsigned int NfeltSpace() noexcept;

                //! Alias to the return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the value actually stored.
                using storage_type = std::decay_t<return_type>;

                //! Friendship to fiber manager.
                friend FiberList<TypeT>;

                //! Frienship to the only class that needs underlying list of \a FEltSpace.
                template
                <
                    ParameterNS::Type TypeTT,
                    template<ParameterNS::Type> class TimeDependencyT,
                    unsigned int NfeltSpaceTT
                >
                friend struct ::MoReFEM::ParameterNS::FromParameterAtDof;

                static_assert(ParameterNS::Type::matrix != TypeT,
                              "This type of parameter can't deal with Matrix parameter");


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \class doxygen_hide_at_dof_impl_constructor_args
                 *
                 * \param[in] domain \a Domain considered. It is actually unused in this policy, but this is
                 * a staple of policy usage and must be provided.
                 * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
                 * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
                 * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
                 * only for ther Parameter). To save space, it's better if this unknown is in its own numbering subset,
                 * but this is not mandatory.
                 * \param[in] global_vector The vector which includes the actual values at the dofs. The values
                 * at dofs may evolve should this vector change.
                 *
                 */



                /*!
                 * \brief Constructor when Parameter cover only one \a FEltSpace.
                 *
                 * This should be the more frequent case.
                 *
                 * \copydetails doxygen_hide_at_dof_impl_constructor_args
                 * \param[in] felt_space Finite element space that covers the area upon which the parameter should be
                 * defined. This finite element space should cover the underlying \a NumberingSubset of \a global_vector.
                 *
                 */
                explicit AtDof(const Domain& domain,
                               const FEltSpace& felt_space,
                               const Unknown& unknown,
                               const GlobalVector& global_vector);


                /*!
                 * \brief Constructor when Parameter cover two \a FEltSpace: one for mesh dimension and another for
                 * the dimension immediately below.
                 *
                 * \copydetails doxygen_hide_at_dof_impl_constructor_args
                 * \param[in] felt_space_dim Finite element space that covers the area upon which the parameter should be
                 * defined for the dimension of the mesh. This finite element space should cover the underlying
                 * \a NumberingSubset of \a global_vector.
                 * \param[in] felt_space_dim_minus_1 Same as \a felt_space_dim except it covers the dimension minus 1
                 * (e.g. finite element space that cover borders on a 2D mesh).
                 *
                 */
                explicit AtDof(const Domain& domain,
                               const FEltSpace& felt_space_dim,
                               const FEltSpace& felt_space_dim_minus_1,
                               const Unknown& unknown,
                               const GlobalVector& global_vector);


                /*!
                 * \brief Constructor when Parameter cover three \a FEltSpace: one for mesh dimension, one for dimension
                 * minus 1 and the last for dimension minus 2.
                 *
                 * \copydetails doxygen_hide_at_dof_impl_constructor_args
                 * \param[in] felt_space_dim Finite element space that covers the area upon which the parameter should be
                 * defined for the dimension of the mesh. This finite element space should cover the underlying
                 * \a NumberingSubset of \a global_vector.
                 * \param[in] felt_space_dim_minus_1 Same as \a felt_space_dim except it covers the dimension minus 1
                 * (e.g. finite element space that cover borders on a 2D mesh).
                 * \param[in] felt_space_dim_minus_2 Same as \a felt_space_dim_minus_1 for dimension minus 2.
                 *
                 */
                explicit AtDof(const Domain& domain,
                               const FEltSpace& felt_space_dim,
                               const FEltSpace& felt_space_dim_minus_1,
                               const FEltSpace& felt_space_dim_minus_2,
                               const Unknown& unknown,
                               const GlobalVector& global_vector);


                //! Destructor.
                ~AtDof() = default;

                //! Copy constructor.
                AtDof(const AtDof&) = delete;

                //! Move constructor.
                AtDof(AtDof&&) = delete;

                //! Copy affectation.
                AtDof& operator=(const AtDof&) = delete;

                //! Move affectation.
                AtDof& operator=(AtDof&&) = delete;

                ///@}

                /*!
                 * \brief Enables to modify the constant value of a parameter. Disable for this Policy.
                 */
                void SetConstantValue(double value);

            public:

                //! Accessor to global vector.
                const GlobalVector& GetGlobalVector() const noexcept;

                //! Access to unknown covered.
                const Unknown& GetUnknown() const noexcept;

            protected:

                //! Provided here to make the code compile, but should never be called.
                [[noreturn]] return_type GetConstantValueFromPolicy() const;

                //! Get the value.
                return_type GetValueFromPolicy(const local_coords_type& local_coords,
                                               const GeometricElt& geom_elt) const;

                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;



            protected:

                /*!
                 * \brief Whether the parameter varies spatially or not.
                 *
                 * \return False for current policy.
                 */
                bool IsConstant() const noexcept;


                /*!
                 * \brief Write the content of the parameter for which policy is used in a stream.
                 *
                 * \param[in,out] out Stream onto which policy value is written.
                 */
                void WriteFromPolicy(std::ostream& out) const;

            private:

                /*!
                 * \brief  Access to the \a FEltSpace that encloses \a geom_elt.
                 *
                 * \param[in] geom_elt Geometric element at which we seek to evaluate the Parameter value.
                 *
                 * \return Reference to the \a FEltSpace that encloses \a geom_elt.
                 *
                 */
                const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const noexcept;
                /*!
                 * \brief Common part to all constructors.
                 *
                 * \copydetails doxygen_hide_at_dof_impl_constructor_args
                 * \param[in] first_felt_space Any of the \a FEltSpace (this argument is used solely in debug mode
                 * for sanity checks).
                 */
                void Construct(const Domain& domain,
                               const FEltSpace& first_felt_space,
                               const Unknown& unknown,
                               const GlobalVector& global_vector);

            private:

                //! Helper object which keepts tracks of the \a FEltSpace(s) objects considered in the \a Parameter.
                const Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<NfeltSpaceT>&
                    GetFEltSpaceStorage() const noexcept;


            private:


                /*!
                 * \brief Helper object which keepts tracks of the \a FEltSpace(s) objects considered in the \a Parameter.
                 *
                 */
                Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<NfeltSpaceT> felt_space_storage_;

                /*!
                 * \brief Unknown used to enumerate the dofs considered by the parameter.
                 *
                 * Must be scalar if TypeT == ParameterNS::Type::scalar.
                 *
                 */
                const Unknown& unknown_;

                //! Global vector that provides the value at each dof.
                const GlobalVector& global_vector_;

                /*!
                 * \brief Local value computed at a given \a LocalCoords.
                 *
                 * \internal <b><tt>[internal]</tt></b> This must really be seen as a work variable, with fleeting and
                 *  non reliable values.
                 * It's there only to avoid multiple allocations of LocalVectors in case of vectorial parameter.
                 */
                mutable storage_type local_value_;


            };


        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Policy/AtDof/AtDof.hxx"


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_AT_DOF_x_AT_DOF_HPP_
