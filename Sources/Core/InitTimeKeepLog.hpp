///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Jan 2015 14:19:22 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_
# define MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_

# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "Utilities/TimeKeep/TimeKeep.hpp"
# include "Utilities/Filesystem/File.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Result.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Init the time keeper and write its logs in the output directory specified in \a input_parameter_data.
     *
     * This function is to be called early in your main if you want to time keep some information lines.
     *
     * \copydetails doxygen_hide_mpi_param
     * \param[in] result_directory Directory into which all outputs should be written. It is assumed to exist when
     * it is given to this function.
     */
    void InitTimeKeepLog(const Wrappers::Mpi& mpi,
                         const std::string& result_directory);



    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_
