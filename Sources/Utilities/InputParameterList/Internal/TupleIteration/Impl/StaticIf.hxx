///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputParameterListNS
        {


            namespace Impl
            {


                template
                <
                    class NextItemT,
                    class InputParameterT
                >
                constexpr bool Find<true, NextItemT, InputParameterT>::Perform()
                {
                    return true;
                }


                template
                <
                    class NextItemT,
                    class InputParameterT
                >
                constexpr bool Find<false, NextItemT, InputParameterT>::Perform()
                {
                    return NextItemT::template Find<InputParameterT>();;
                }



                //! Specialization when requested value has been found.
                template
                <
                    class EnrichedSectionOrParameterT,
                    unsigned int IndexT,
                    class NextItemT
                >
                template<class TupleT, class InputParameterT>
                void ExtractValueFromInputParameter<true, EnrichedSectionOrParameterT, IndexT, NextItemT>
                ::Perform(const TupleT& tuple,
                          const InputParameterT*& parameter_if_found)
                {
                    const auto& item = std::get<IndexT>(tuple);
                    EnrichedSectionOrParameterT::ExtractValue(item, parameter_if_found);
                }


                //! Specialization when requested value has not been found.
                template
                <
                    class EnrichedSectionOrParameterT,
                    unsigned int IndexT,
                    class NextItemT
                >
                template<class TupleT, class InputParameterT>
                void ExtractValueFromInputParameter<false, EnrichedSectionOrParameterT, IndexT, NextItemT>
                ::Perform(const TupleT& tuple,
                          const InputParameterT*& parameter_if_found)
                {
                    NextItemT::template ExtractValue<InputParameterT>(tuple, parameter_if_found);
                }



                template
                <
                    class TupleIterationT
                >
                template<class SectionContentT, class InputParameterT>
                void ExtractValueFromSection<false, TupleIterationT>
                ::Perform(const SectionContentT& ,
                          const InputParameterT*& )
                {
                    // Do nothing.
                };

                template
                <
                    class TupleIterationT
                >
                template<class SectionContentT, class InputParameterT>
                void ExtractValueFromSection<true, TupleIterationT>
                ::Perform(const SectionContentT& item,
                          const InputParameterT*& parameter_if_found)
                {
                    TupleIterationT::ExtractValue(item,
                                                  parameter_if_found);

                }


            } // namespace Impl


        } // namespace InputParameterListNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_STATIC_IF_HXX_
