///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron4.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Tetrahedron4 geometric element read in a mesh.
     *
     * We are not considering here a generic Tetrahedron4 object (that's the role of MoReFEM::RefGeomEltNS::Tetrahedron4), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Tetrahedron4 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron4>
    {
    public:

        //! Minimal constructor: only the GeometricMeshRegion is provided.
        explicit Tetrahedron4(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Tetrahedron4(unsigned int mesh_unique_id,
                              const Coords::vector_unique_ptr& mesh_coords_list,
                              std::istream& stream);

        //! Constructor from vector of coords.
        explicit Tetrahedron4(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Tetrahedron4() override;

        //! Copy constructor.
        Tetrahedron4(const Tetrahedron4&) = default;

        //! Move constructor.
        Tetrahedron4(Tetrahedron4&&) = default;

        //! Copy affectation.
        Tetrahedron4& operator=(const Tetrahedron4&) = default;

        //! Move affectation.
        Tetrahedron4& operator=(Tetrahedron4&&) = default;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Tetrahedron4& StaticRefGeomElt();



    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON4_HPP_
