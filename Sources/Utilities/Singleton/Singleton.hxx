///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 17:41:03 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HXX_
# define MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        template<class T>
        T* Singleton<T>::instance_ = nullptr;


        template<class T>
        bool Singleton<T>::destroyed_ = false;


        template<class T>
        std::mutex Singleton<T>::singleton_mutex_;


        template<class T>
        void Singleton<T>::Destroy()
        {
            instance_ = nullptr;
            destroyed_ = true;
        }


        template<class T>
        template<class... Args>
        T& Singleton<T>::CreateOrGetInstance(Args&&... args)
        {
            std::lock_guard<std::mutex> lock(GetNonCstSingletonMutex());

            if (!instance_)
            {
                // Check for dead reference.
                if (destroyed_)
                    OnDeadReference();
                else
                {
                    // First call initialize.
                    Create(std::forward<Args>(args)...);
                }
            }

            return *instance_;
        }



        template<class T>
        T& Singleton<T>::GetInstance()
        {
            if (!instance_) // whichever the case it is an anormal stance of the program...
            {
                // Check for dead reference.
                if (destroyed_)
                    OnDeadReference();
                else
                    throw ExceptionNS::Singleton::NotYetCreated(T::ClassName(), __FILE__, __LINE__);
            }

            return *instance_;
        }


        template<class T>
        template<class... Args>
        void Singleton<T>::Create(Args&&... args)
        {
            assert(!instance_);
            instance_ = new T(std::forward<Args>(args)...);

            int error_code = std::atexit(Destroy); // to destroy the singleton at the end of the program.

            if (error_code != 0)
                throw Exception("Failing to register " + T::ClassName() + " in atexit destruction stack.",
                                __FILE__, __LINE__);
        }


        template<class T>
        std::mutex& Singleton<T>::GetNonCstSingletonMutex()
        {
            return singleton_mutex_;
        }


        template<class T>
        void Singleton<T>::OnDeadReference()
        {
            throw ExceptionNS::Singleton::DeadReference(T::ClassName(), __FILE__, __LINE__);
        }


    } // namespace Utilities


} // namespace Singleton


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_SINGLETON_x_SINGLETON_HXX_
