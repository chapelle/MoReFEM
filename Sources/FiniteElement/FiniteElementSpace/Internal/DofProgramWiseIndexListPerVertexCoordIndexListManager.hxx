///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            template<class SectionT>
            void DofProgramWiseIndexListPerVertexCoordIndexListManager::Create(const SectionT& section,
                                                                           const GodOfDof& god_of_dof)
            {
                namespace ipl = Internal::InputParameterListNS;

                decltype(auto) felt_space_index = ipl::ExtractParameter<typename SectionT::FEltSpaceIndex>(section);
                decltype(auto) numbering_subset_index =
                    ipl::ExtractParameter<typename SectionT::NumberingSubsetIndex>(section);

                Create(section.GetUniqueId(),
                       god_of_dof,
                       felt_space_index,
                       numbering_subset_index);
            }


            inline const auto& DofProgramWiseIndexListPerVertexCoordIndexListManager::GetStorage() const noexcept
            {
                return list_;
            }


            inline auto& DofProgramWiseIndexListPerVertexCoordIndexListManager::GetNonCstStorage() noexcept
            {
                return list_;

            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HXX_
