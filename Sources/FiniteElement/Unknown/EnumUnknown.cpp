///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Mar 2015 16:50:27 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

# include <ostream>

# include "FiniteElement/Unknown/EnumUnknown.hpp"


namespace std
{

    
    std::ostream& operator<<(std::ostream& stream, const ::MoReFEM::UnknownNS::Nature nature)
    {
        switch(nature)
        {
            case ::MoReFEM::UnknownNS::Nature::scalar:
                stream << "scalar";
                break;
            case ::MoReFEM::UnknownNS::Nature::vectorial:
                stream << "vectorial";
                break;
        }
        
        return stream;
    }

    
} // namespace std


/// @} // addtogroup FiniteElementGroup

