///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Sep 2015 11:23:48 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        template<unsigned int IndexT>
        const std::string& DirichletBoundaryCondition<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("EssentialBoundaryCondition", IndexT);
            return ret;
        };


        template<unsigned int IndexT>
        constexpr unsigned int DirichletBoundaryCondition<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_DIRICHLET_BOUNDARY_CONDITION_x_DIRICHLET_BOUNDARY_CONDITION_HXX_
