///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 12:00:59 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HPP_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HPP_

# include "Parameters/ParameterType.hpp"

# include "Core/InputParameter/Crtp/Section.hpp"

# include "Core/InputParameter/Parameter/Fiber/Impl/Fiber.hpp"


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace BaseNS
        {

            /*!
             * \brief Common base class from which all InputParameter::Fiber should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            template<ParameterNS::Type TypeT>
            class Fiber
            { };



        } // namespace BaseNS



        /*!
         * \copydoc doxygen_hide_core_input_parameter_list_section_with_index
         * \tparam TypeT Type of the ]a parameter.
         */
        template
        <
            unsigned int IndexT,
            ParameterNS::Type TypeT
        >
        struct Fiber
        : public Crtp::Section<Fiber<IndexT, TypeT>, NoEnclosingSection>,
        public BaseNS::Fiber<TypeT>
        {
            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;

            /*!
             * \brief Return the name of the section in the input parameter.
             *
             * e.g. 'Fiber1' for IndexT = 1.
             *
             * \return Name of the section in the input parameter.
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = Fiber<IndexT, TypeT>;

            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Ensight file from which data are read.
             */
            struct EnsightFile
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<EnsightFile, self, FiberNS::EnsightFile::storage_type>,
            public FiberNS::EnsightFile
            { };



            /*!
             * \brief Index of the \a Domain onto which fiber is defined. It is expected this parameter
             * is compatible with the fiber file.
             */
            struct DomainIndex
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<DomainIndex, self, FiberNS::DomainIndex::storage_type>,
            public FiberNS::DomainIndex
            { };


            /*!
             * \brief Index of the finite element space  onto which fiber is defined.
             */
            struct FEltSpaceIndex
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<FEltSpaceIndex, self, FiberNS::FEltSpaceIndex::storage_type>,
            public FiberNS::FEltSpaceIndex
            { };



            /*!
             * \brief Name of the (fictitious) unknown used to define the parameter.
             *
             * A dof might only be defined in relation to a dof.
             */
            struct UnknownName
            : public  Utilities::InputParameterListNS::Crtp::InputParameter<UnknownName, self, FiberNS::UnknownName::storage_type>,
            public FiberNS::UnknownName
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                EnsightFile,
//                Nature,
                DomainIndex,
                FEltSpaceIndex,
                UnknownName
            >;


        private:

            //! Content of the section.
            section_content_type section_content_;



        }; // struct Fiber


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/InputParameter/Parameter/Fiber/Fiber.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_FIBER_x_FIBER_HPP_
