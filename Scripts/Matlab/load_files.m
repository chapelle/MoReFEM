function [data] = load_files(directory, name, extension, num)
    files = dir(strcat(directory,name,'*',extension));
        
    data = [];

    if (num > 0)
        number = num;
    else
        number = length(files);
    end
    
    for k = 1:number
        if (size(load(strcat(directory,files(k).name), '-ascii'),2) > 1)
            data = [data ; load(strcat(directory,files(k).name), '-ascii') ];
        else
            data = [data , load(strcat(directory,files(k).name), '-ascii') ];
        end
    end    
end