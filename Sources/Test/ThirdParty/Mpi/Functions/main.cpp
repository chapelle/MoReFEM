/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/Mpi/ErrorHandling/InputParameterList.hpp"


using namespace MoReFEM;


namespace // anonymous
{
    
    
    //! Check Gather() and AllGather() work as expected.
    void TestGather(const Wrappers::Mpi& mpi);
    
    //! Check Gatherv() and AllGatherv() work as expected.
    void TestGatherv(const Wrappers::Mpi& mpi);
    
    
    
} // namespace anonymous




int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = TestNS::InputParameterListNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
        
        const auto& mpi = morefem_data.GetMpi();
        
        assert(mpi.Nprocessor<int>() == 4 && "This test has been written for four processors specifically!");

        try
        {
            TestGather(mpi);
            TestGatherv(mpi);
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());            
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }        
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}



namespace // anonymous
{
    
    
    void TestGather(const Wrappers::Mpi& mpi)
    {
        const auto rank = mpi.GetRank<int>();
        
        std::vector<int> sent_data;
        
        if (rank == 0)
            sent_data = { 1, 2, 3 };
        if (rank == 1)
            sent_data = { 11, 12, 13 };
        if (rank == 2)
            sent_data = { 21, 22, 23 };
        if (rank == 3)
            sent_data = { 31, 32, 33 };
        
        std::vector<int> gathered_data_on_root_processor;
        
        mpi.Gather(sent_data, gathered_data_on_root_processor);
        
        std::vector<int> full_sequence = { 1, 2, 3, 11, 12, 13, 21, 22, 23, 31, 32, 33 };
        
        if (mpi.IsRootProcessor())
        {
            if (gathered_data_on_root_processor != full_sequence)
                throw Exception("Gathered vector on root processor is not what was expected.",
                                __FILE__, __LINE__);
        }
        
        std::vector<int> gathered_data;
        mpi.AllGather(sent_data, gathered_data);
        
        if (gathered_data != full_sequence)
        {
            std::ostringstream oconv;
            oconv << "Gathered vector on processor ranked " << rank << " is not what was expected: ";
            Utilities::PrintContainer(gathered_data, oconv);
            
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }
    
    
    void TestGatherv(const Wrappers::Mpi& mpi)
    {
        const auto rank = mpi.GetRank<int>();
        
        std::vector<int> sent_data;
        
        if (rank == 0)
            sent_data = { 1 };
        if (rank == 1)
            sent_data = { 11, 12 };
        if (rank == 2)
            sent_data = { 21, 22, 23 };
        if (rank == 3)
            sent_data = { 31, 32, 33, 34 };
        
        std::vector<int> full_sequence = { 1, 11, 12, 21, 22, 23, 31, 32, 33, 34 };
        
        std::vector<int> gathered_data_on_root_processor;
        
        mpi.Gatherv(sent_data, gathered_data_on_root_processor);
        
        if (mpi.IsRootProcessor())
        {
            if (gathered_data_on_root_processor != full_sequence)
                throw Exception("Gathered vector on root processor is not what was expected.",
                                __FILE__, __LINE__);
        }
        
        std::vector<int> gathered_data;
        
        mpi.AllGatherv(sent_data, gathered_data);
        
        if (gathered_data != full_sequence)
        {
            std::ostringstream oconv;
            oconv << "Gathered vector on processor ranked " << rank << " is not what was expected: ";
            Utilities::PrintContainer(gathered_data, oconv);
            
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }

    }
    
    
} // namespace anonymous


