///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {


                inline const LocalFEltSpacePerRefLocalFEltSpace& InternalStorage::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
                {
                    return felt_list_per_ref_felt_space_;
                }


                inline LocalFEltSpacePerRefLocalFEltSpace& InternalStorage::GetNonCstFEltListPerRefLocalFEltSpace() noexcept
                {
                    return const_cast<LocalFEltSpacePerRefLocalFEltSpace&>(GetLocalFEltSpacePerRefLocalFEltSpace());
                }


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HXX_
