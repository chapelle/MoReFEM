///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:18:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HPP_

# include <memory>
# include <vector>

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"
# include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefFEltNS
        {


            /*!
             * \brief RefFEltInFEltSpace is the description of a finite element related to a specific RefGeomElt,
             * a specific Unknown and a numbering subset.
             *
             * For instance, RefFEltInFEltSpace could describe the tuple ('Triangle3', 'Scalar unknown',
             * 'numbering subset 1').
             *
             * \internal <b><tt>[internal]</tt></b> This class is intended to work within the FEltSpace and should be
             * manipulated by a library developer; it basically enrich the BasicRefFElt with some data related to a
             * \a FEltSpace. What a developer should rather use is a RefFEltInLocalOperator, which is built upon
             * current class and adds several data related to the operator in which it is intended to be used.
             *
             */
            class RefFEltInFEltSpace final
            {

            public:

                //! Alias to shared pointer.
                using const_shared_ptr = std::shared_ptr<const RefFEltInFEltSpace>;

                //! Alias to vector of shared_pointer.
                using vector_const_shared_ptr = std::vector<const_shared_ptr>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] basic_ref_felt Description of the reference finite element.
                 * \param[in] extended_unknown Unknown and numbering subset related to the finite element.
                 * \param[in] mesh_dimension Highest dimension of the mesh onto which the finite element space is defined. May
                 * be higher than \a felt_space_dimension.
                 * \param[in] felt_space_dimension Dimension of the finite element space in which the new object is built.
                 */
                explicit RefFEltInFEltSpace(const BasicRefFElt& basic_ref_felt,
                                            const ExtendedUnknown& extended_unknown,
                                            unsigned int mesh_dimension,
                                            unsigned int felt_space_dimension);


                //! Destructor.
                ~RefFEltInFEltSpace();

                //! Copy constructor.
                RefFEltInFEltSpace(const RefFEltInFEltSpace&) = delete;

                //! Move constructor.
                RefFEltInFEltSpace(RefFEltInFEltSpace&&) = default;

                //! Affectation.
                RefFEltInFEltSpace& operator=(const RefFEltInFEltSpace&) = delete;

                //! Affectation.
                RefFEltInFEltSpace& operator=(RefFEltInFEltSpace&&) = delete;

                ///@}

            public:


                //! Access to the related BasicRefFElt.
                const BasicRefFElt& GetBasicRefFElt() const noexcept;

                //! Returns the number of nodes.
                unsigned int Nnode() const noexcept;

                //! Returns the number of dofs.
                unsigned int Ndof() const noexcept;

                //! Access related Unknown/numbering subset pair.
                const ExtendedUnknown& GetExtendedUnknown() const noexcept;

                //! Access to the dimension of the mesg in which current finite element is built.
                unsigned int GetMeshDimension() const noexcept;

                //! Access to the dimension of the finite element space for which current reference finite element is built.
                unsigned int GetFEltSpaceDimension() const noexcept;

                //! Returns the number of components: 1 if the unknown is scalar, dimension_ otherwise.
                unsigned int Ncomponent() const;


            private:

                /*!
                 * \brief Access to the reference finite element.
                 */
                const BasicRefFElt& basic_ref_felt_;

                //! Unknown/numbering subset pair.
                const ExtendedUnknown& extended_unknown_;

                /*!
                 * \brief Highest dimension of the mesh onto which the finite element space is defined.
                 *
                 * May be higher than \a felt_space_dimension.
                 */
                const unsigned int mesh_dimension_;

                //! Dimension of the finite element space for which current reference finite element is built.
                const unsigned int felt_space_dimension_;

            };


        } // namespace RefFEltNS


    } //  namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_F_ELT_IN_F_ELT_SPACE_HPP_
