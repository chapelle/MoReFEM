/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Geometry/DomainListInCoords/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {


            Model::Model(const morefem_data_type& morefem_data)
            : parent(morefem_data, create_domain_list_for_coords::yes),
            output_directory_(morefem_data.GetResultDirectory())
            { }

            
            namespace // anonymous
            {
                
                
                void CheckValue(std::string&& name,
                                unsigned int expected_value,
                                unsigned int computed_value);
                
                void ParallelCheckValue(const Wrappers::Mpi& mpi,
                                        std::string&& name,
                                        unsigned int expected_value,
                                        unsigned int processor_computed_value);
                
                
                
            } // namespace anonymous

            
            void Model::SupplInitialize(const morefem_data_type& morefem_data)
            {
                static_cast<void>(morefem_data);
                
                const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
                
                const auto& mesh = god_of_dof.GetGeometricMeshRegion();
                
                const auto& coords_list = mesh.GetProcessorWiseCoordsList();
                
                decltype(auto) domain_manager = DomainManager::GetInstance();
                
                decltype(auto) domain_volume =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);
                const auto domain_volume_unique_id = domain_volume.GetUniqueId();
                unsigned int Ncoords_in_volume = 0;
                
                decltype(auto) domain_ring =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::ring), __FILE__, __LINE__);
                const auto domain_ring_unique_id = domain_ring.GetUniqueId();
                unsigned int Ncoords_in_ring = 0;
                
                decltype(auto) domain_exterior =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::exterior_surface), __FILE__, __LINE__);
                const auto domain_exterior_unique_id = domain_exterior.GetUniqueId();
                unsigned int Ncoords_in_exterior = 0;
                
                decltype(auto) domain_interior =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::interior_surface), __FILE__, __LINE__);
                const auto domain_interior_unique_id = domain_interior.GetUniqueId();
                unsigned int Ncoords_in_interior = 0;
                
                // Domains through lightweight domain list
                decltype(auto) domain_exterior_and_ring = domain_manager.GetDomain(5, __FILE__, __LINE__);
                const auto domain_exterior_and_ring_unique_id = domain_exterior_and_ring.GetUniqueId();
                unsigned int Ncoords_in_exterior_and_ring = 0;
                
                decltype(auto) domain_interior_and_ring = domain_manager.GetDomain(6, __FILE__, __LINE__);
                const auto domain_interior_and_ring_unique_id = domain_interior_and_ring.GetUniqueId();
                unsigned int Ncoords_in_interior_and_ring = 0;
                
                for (const auto& coords_ptr : coords_list)
                {
                    assert(!(!coords_ptr));
                    
                    const auto& coords = *coords_ptr;
                    
                    if (coords.IsInDomain(domain_volume_unique_id))
                        ++Ncoords_in_volume;
                    
                    if (coords.IsInDomain(domain_ring_unique_id))
                        ++Ncoords_in_ring;
                    
                    if (coords.IsInDomain(domain_exterior_unique_id))
                        ++Ncoords_in_exterior;
                    
                    if (coords.IsInDomain(domain_interior_unique_id))
                        ++Ncoords_in_interior;
                    
                    if (coords.IsInDomain(domain_exterior_and_ring_unique_id))
                        ++Ncoords_in_exterior_and_ring;
                    
                    if (coords.IsInDomain(domain_interior_and_ring_unique_id))
                        ++Ncoords_in_interior_and_ring;
                }
                
                decltype(auto) mpi = parent::GetMpi();
                const auto Nprocessor = mpi.Nprocessor<int>();
                
                const auto Ncoords_in_volume_program_wise = NcoordsInDomain<MpiScale::program_wise>(GetMpi(),
                                                                                                    domain_exterior,
                                                                                                    mesh);
                
                const auto Ncoords_in_volume_processor_wise = NcoordsInDomain<MpiScale::processor_wise>(GetMpi(),
                                                                                                        domain_exterior,
                                                                                                        mesh);

                
                if (Nprocessor == 1)
                {
                    CheckValue("Ncoords_in_volume", 48, Ncoords_in_volume);
                    CheckValue("Ncoords_in_ring", 8, Ncoords_in_ring);
                    CheckValue("Ncoords_in_exterior", 24, Ncoords_in_exterior);
                    CheckValue("Ncoords_in_interior", 24, Ncoords_in_interior);
                    CheckValue("Ncoords_in_exterior_and_ring", 28, Ncoords_in_exterior_and_ring);
                    CheckValue("Ncoords_in_interior_and_ring", 28, Ncoords_in_interior_and_ring);
                    CheckValue("Ncoords_in_volume_program_wise", 24, Ncoords_in_volume_program_wise);
                    CheckValue("Ncoords_in_volume_processor_wise", 24, Ncoords_in_volume_processor_wise);
                    
                    
                }
                else if (Nprocessor == 4)
                {
                    // We can't really compare processor by processor, as parallel repartition may differ from one
                    // compiler to another. So we gather them all and check nothing is lost.
                    
                    ParallelCheckValue(mpi, "Ncoords_in_volume", 48, Ncoords_in_volume);
                    ParallelCheckValue(mpi, "Ncoords_in_ring", 8, Ncoords_in_ring);
                    ParallelCheckValue(mpi, "Ncoords_in_exterior", 24, Ncoords_in_exterior);
                    ParallelCheckValue(mpi, "Ncoords_in_interior", 24, Ncoords_in_interior);
                    ParallelCheckValue(mpi, "Ncoords_in_exterior_and_ring", 28, Ncoords_in_exterior_and_ring);
                    ParallelCheckValue(mpi, "Ncoords_in_interior_and_ring", 28, Ncoords_in_interior_and_ring);
                    CheckValue("Ncoords_in_volume_program_wise", 24, Ncoords_in_volume_program_wise); // not a mistake!
                    ParallelCheckValue(mpi, "Ncoords_in_volume_processor_wise", 24, Ncoords_in_volume_processor_wise);

                 
                }
                else
                    throw Exception("This test was written to be checked against 1 or 4 processors.",
                                    __FILE__, __LINE__);
                
            }
            
            
            namespace // anonymous
            {
                
                
                void CheckValue(std::string&& name,
                                unsigned int expected_value,
                                unsigned int computed_value)
                {
                    if (expected_value != computed_value)
                    {
                        std::ostringstream oconv;
                        oconv << "Expected value for " << name << " was " << expected_value << " but computation "
                        "yielded " << computed_value;
                        
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                }
                
                
                void ParallelCheckValue(const Wrappers::Mpi& mpi,
                                        std::string&& name,
                                        unsigned int expected_value,
                                        unsigned int processor_computed_value)
                {
                    auto sum = mpi.ReduceOnRootProcessor(processor_computed_value, Wrappers::MpiNS::Op::Sum);
                    
                    if (mpi.IsRootProcessor() && expected_value != sum)
                    {
                        std::ostringstream oconv;
                        oconv << "Expected value for " << name << " was " << expected_value << " but computation "
                        "yielded " << sum;
                        
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                }
                
                
                
            } // namespace anonymous



            void Model::Forward()
            { }


            void Model::SupplFinalizeStep()
            { }


            void Model::SupplFinalize()
            { }


    
        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM
