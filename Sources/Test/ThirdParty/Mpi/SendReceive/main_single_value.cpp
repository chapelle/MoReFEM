/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 Sep 2017 15:59:08 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Seldon/Seldon.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/Mpi/SendReceive/InputParameterList.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    
    //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = TestNS::MpiSendReceiveNS::InputParameterList;
    
    try
    {
        MoReFEMData<InputParameterList> morefem_data(argc, argv);
                
        const auto& mpi = morefem_data.GetMpi();
        
        const auto rank = mpi.GetRank<int>();
        const auto Nproc = mpi.Nprocessor<int>();
        
        try
        {
            namespace ipl = Utilities::InputParameterListNS;
            
            double token = std::numeric_limits<double>::lowest();
            
            if (rank != 0)
            {
                token = mpi.Receive<double>(static_cast<unsigned int>(rank - 1));
                
                std::cout << "Process " << rank << " received token " << token;
                std::cout << " from process " << rank - 1 << std::endl;
            }
            else
            {
                // Set the token's value if you are process 0
                token = 1.245;
            }
            
            mpi.Send(static_cast<unsigned int>((rank + 1) % Nproc),
                     token);
            
            // Now process 0 can receive from the last process.
            if (rank == 0)
            {
                token = mpi.Receive<double>(static_cast<unsigned int>(Nproc - 1));

                std::cout << "Process " << rank << " received token " << token;
                std::cout << " from process " << Nproc - 1 << std::endl;
            }
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());            
        }
        catch(Seldon::Error& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.What());
        }        
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputParameterList>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}

