///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:25 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <iostream>
#include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"


namespace MoReFEM
{


    LocalNode::LocalNode(RefGeomEltNS::TopologyNS::LocalInterface&& local_interface,
                         unsigned int index,
                         const LocalCoords& local_coords)
    : local_interface_(std::move(local_interface)),
    index_(index),
    local_coords_(local_coords)
    { }

    
    
    void LocalNode::Print(std::ostream& out) const
    {
        out << "LocalNode " << GetIndex() << " [" << GetLocalInterface().GetNature() << "]";
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
