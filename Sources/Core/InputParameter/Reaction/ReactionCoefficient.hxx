///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 1 Jul 2015 11:51:49 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_REACTION_COEFFICIENT_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_REACTION_COEFFICIENT_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {


        namespace ReactionNS
        {




        } // namespace ReactionNS


    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_REACTION_x_REACTION_COEFFICIENT_HXX_
