///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace MeshNS
        {
            
            
           
            const std::string& GeometricMeshRegionManager::ClassName()
            {
                static std::string ret("GeometricMeshRegionManager");
                return ret;
            }
            
            
            GeometricMeshRegionManager::GeometricMeshRegionManager()
            {
                storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            }
            
            
            void GeometricMeshRegionManager::Create(const unsigned int unique_id,
                                                    const std::string& mesh_file,
                                                    unsigned dimension,
                                                    ::MoReFEM::MeshNS::Format format,
                                                    const double space_unit,
                                                    GeometricMeshRegion::BuildEdge do_build_edge,
                                                    GeometricMeshRegion::BuildFace do_build_face,
                                                    GeometricMeshRegion::BuildVolume do_build_volume,
                                                    GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals)
            {
                // make_unique is not accepted here: it makes the code yell about private status of the constructor
                // with both clang and gcc.
                GeometricMeshRegion* buf = new GeometricMeshRegion(unique_id,
                                                                   mesh_file,
                                                                   dimension,
                                                                   format,
                                                                   space_unit,
                                                                   do_build_edge,
                                                                   do_build_face,
                                                                   do_build_volume,
                                                                   do_build_pseudo_normals);
                
                InsertMesh<is_unique_id_known::no>(buf);
            }
            
            
            void GeometricMeshRegionManager
            ::Create(unsigned int dimension,
                     const double space_unit,
                     GeometricElt::vector_shared_ptr&& unsort_element_list,
                     Coords::vector_unique_ptr&& coords_list,
                     MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                     GeometricMeshRegion::BuildEdge do_build_edge,
                     GeometricMeshRegion::BuildFace do_build_face,
                     GeometricMeshRegion::BuildVolume do_build_volume,
                     GeometricMeshRegion::BuildPseudoNormals do_build_pseudo_normals)
            {
                if (unsort_element_list.empty())
                    throw Exception("A mesh must contain at least one GeometricElement", __FILE__, __LINE__);
                
                const auto& any_elt_ptr = unsort_element_list.back();
                assert(!(!any_elt_ptr));
                const auto unique_id = any_elt_ptr->GetMeshIdentifier();

                assert(std::all_of(unsort_element_list.cbegin(),
                                   unsort_element_list.cend(),
                                   [unique_id](const auto& elt_ptr)
                                   {
                                       assert(!(!elt_ptr));
                                       return elt_ptr->GetMeshIdentifier() == unique_id;
                                   }));
                                
                GeometricMeshRegion* buf = new GeometricMeshRegion(unique_id,
                                                                   dimension,
                                                                   space_unit,
                                                                   std::move(unsort_element_list),
                                                                   std::move(coords_list),
                                                                   std::move(mesh_label_list),
                                                                   do_build_edge,
                                                                   do_build_face,
                                                                   do_build_volume,
                                                                   do_build_pseudo_normals);
                
                InsertMesh<is_unique_id_known::yes>(buf);
            }

            
            const GeometricMeshRegion& GeometricMeshRegionManager::GetMesh(unsigned int unique_id) const
            {
                decltype(auto) storage = GetStorage();
                auto it = storage.find(unique_id);
                
                assert(it != storage.cend());
                assert(!(!(it->second)));
                
                return *(it->second);
            }
            
            
            void WriteInterfaceListForEachMesh(const std::map<unsigned int, std::string>& mesh_output_directory_storage)
            {
                for (const auto& pair : mesh_output_directory_storage)
                    WriteInterfaceList(pair);
            }
            
            
            unsigned int GeometricMeshRegionManager::GenerateUniqueId()
            {
                auto& unique_id_list = GetNonCstUniqueIdList();
                
                unsigned int ret = 0u;
                
                if (!unique_id_list.empty())
                    ret = *(unique_id_list.rbegin()) + 1u;
                
                unique_id_list.insert(ret);
                
                return ret;
            }
            
            
        } // namespace MeshNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
