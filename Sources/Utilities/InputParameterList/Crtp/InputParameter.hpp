///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Aug 2013 12:19:17 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HPP_


# include <unordered_map>
# include <tuple>
# include "Utilities/Miscellaneous.hpp"
# include "Utilities/Containers/Vector.hpp"
# include "Utilities/String/String.hpp"
# include "Utilities/InputParameterList/OpsFunction.hpp"
# include "Utilities/InputParameterList/Definitions.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Traits/Traits.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/OpsType.hpp"
# include "Utilities/InputParameterList/Internal/TupleIteration/Impl/TupleItem.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputParameterListNS
        {


            namespace Crtp
            {


                /*!
                 * \brief Generic class from which each InputParameter should derive.
                 *
                 * \tparam DerivedT To enact CRTP behaviour.
                 * \tparam EnclosingSectionT Type of the enclosing section. Choose NoEnclosingSection if the parameter is
                 * at root level.
                 * \tparam ReturnTypeT Type that will be used to store the value read in the input parameter file.
                 */
                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                class InputParameter
                {
                public:

                    //! Type of the input parameter.
                    using return_type = ReturnTypeT;

                    //! Type of the enclosing section.
                    using enclosing_section = EnclosingSectionT;

                    //! Specifies the nature is 'parameter' and not 'section'.
                    static constexpr Nature GetNature() noexcept;

                    /*!
                     * \brief Type compatible with Ops interface.
                     *
                     * Generally the same as return_type, but might differ for some intergal constant types
                     * (unsigned int not supported by Ops for instance; see ops_type for more details).
                     *
                     */
                    using ops_type =
                        typename Internal::InputParameterListNS::Traits::InputParameter<return_type>::ops_type;

                    /*!
                     * \return The input parameter.
                     *
                     * Shouldn't be called anywhere but within InputParameterList class: this class gets safeguards to ensure the
                     * variable was properly initialized.
                     *
                     * \internal <b><tt>[internal]</tt></b> There is a reason why this method is NOT called Value(): as explained in the class
                     * description, the purpose of this class is to spawn a new class for each input parameter
                     * to consider. A user might see fit to name such an input parameter class Value, which would
                     * trigger compilation error as the compiler would think the constructor is called instead of
                     * the present method. On the contrary, I would raise a brow if a user would want to name
                     * one of its input parameter GetTheValue...
                     */
                    typename Utilities::ConstRefOrValue<return_type>::type GetTheValue() const;

                    //! Set the value.
                    void SetValue(ops_type value);

                    //! Tells the value has been called and has therefore probably be used.
                    void SetAsUsed() const noexcept;

                    //! Tells whether the value has been used at least once.
                    bool IsUsed() const noexcept;

                    //! Induce the identifier from section and name.
                    static const std::string& GetIdentifier();

                private:

                    //! Value of the input parameter stored.
                    return_type value_;

                    //! Whether the value has been called at least once.
                    mutable bool is_used_ = false;
                };


            } // namespace Crtp


        } // namespace InputParameterListNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputParameterList/Crtp/InputParameter.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_PARAMETER_LIST_x_CRTP_x_INPUT_PARAMETER_HPP_
