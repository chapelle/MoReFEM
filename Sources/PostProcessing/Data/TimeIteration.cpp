///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:38:17 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup PostProcessingGroup
/// \addtogroup PostProcessingGroup
/// \{

#include <sstream>

#include "PostProcessing/Exceptions/Exception.hpp"
#include "PostProcessing/Data/TimeIteration.hpp"


namespace MoReFEM
{
    
    
    namespace PostProcessingNS
    {
        
        
        namespace Data
        {
            
            
            TimeIteration::TimeIteration(const std::string& line)
            {
                std::istringstream iconv(line);
                
                iconv >> time_iteration_;
                
                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                
                iconv.ignore(); // for ';'
                
                iconv >> time_;
                
                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                
                iconv.ignore(); // for ';'
                
                iconv >> numbering_subset_id_;
                
                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
                
                iconv.ignore(); // for ';'
                
                iconv >> solution_filename_;
                
                if (iconv.fail())
                    throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
            }
            
            
        } // namespace Data
        
        
    } // namespace PostProcessingNS
    
    
} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
