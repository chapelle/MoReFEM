///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

# include "Core/TimeManager/TimeManager.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ActiveStressPolicy/None.hpp"

namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        namespace SecondPiolaKirchhoffStressTensorNS
        {
            
            
            namespace ActiveStressPolicyNS
            {
                
                
                None::None(const unsigned int mesh_dimension,
                           const unsigned int Nnode,
                           const unsigned Nquad_point,
                           const TimeManager& time_manager,
                           input_active_stress_policy_type* input_active_stress_policy)
                {
                    static_cast<void>(mesh_dimension);
                    static_cast<void>(Nnode);
                    static_cast<void>(Nquad_point);
                    static_cast<void>(time_manager);
                    static_cast<void>(input_active_stress_policy);
                }
                
                
            } // namespace ActiveStressPolicyNS
            
            
        } // namespace SecondPiolaKirchhoffStressTensorNS
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
