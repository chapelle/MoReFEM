add_library(Seldon ${LIBRARY_TYPE} "")

target_sources(Seldon
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/lib/CheckDim.cpp"
        "${CMAKE_CURRENT_LIST_DIR}/share/Errors.cxx"
        "${CMAKE_CURRENT_LIST_DIR}/share/Common.cxx"
        "${CMAKE_CURRENT_LIST_DIR}/share/MatrixFlag.cxx"
)

# Suppress all warnings from this third party library. PRIVATE is important not to propagate that to the MoReFEM libraries!
target_compile_options(Seldon PRIVATE -w)

install(TARGETS Seldon
        RUNTIME DESTINATION ${MOREFEM_INSTALL_DIR_EXE}
        LIBRARY DESTINATION ${MOREFEM_INSTALL_DIR_LIB}
        ARCHIVE DESTINATION ${MOREFEM_INSTALL_DIR_LIB})

target_link_libraries(${MOREFEM_UTILITIES} Seldon)