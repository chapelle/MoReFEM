# This file refers to the clang built manually on top of LLVM, reather than the default one for instance found in Apple XCode.

def WarningFlags():
    '''
    List of warning flags to consider when compiling with clang.

    The idea is to use every warning available for clang except a handful ones that are too cumbersome.
    Those are:
    . -Wno-c++98-compat, -Wno-c++98-compat-pedantic: MoReFEM is resolutely C++ 14 in style and doesn't bother with C++ 98 compatibility.
    . -Wno-padded: This warning tells each time it has to pad a class to make it a multiple of pointer size. The issue is that it tells this even if you can't do better (for instance a class with only one boolean attribute); it would be useful if it guides the user toward more efficiency (for instance suggesting to replace bool, double, bool by bool, bool, double which might take less space in storage).
    . -Wno-exit-time-destructors, -Wno-exit-time-destructors: these would triggger warnings due to some design pattern I used (object factories).
    . -Wno-documentation-unknown-command: BEcause clang is not up-to-date with current Doxygen syntax.    
    '''


    return ('-Weverything',
            '-Wno-c++98-compat',
            '-Wno-c++98-compat-pedantic',
            '-Wno-padded',
            '-Wno-exit-time-destructors',
            '-Wno-global-constructors',
            '-Wno-documentation',
            '-Wno-documentation-unknown-command',
            '-Wno-undefined-func-template')


def DebugMacroFlags():
    '''Macro flags that are present only in debug mode.
    
    Only macros specific to compiler are given here; NDEBUG for instance is not and is handled elsewhere.
    
    For instance here we use '_LIBCPP_DEBUG2=0' which is a libc++ (STL used by clang) macro to tells when there 
    are overflows in array indexes.
    '''
    return ('_LIBCPP_DEBUG2=0', )

        
def MiscellaneousFlags(env):
    '''Any flag not yet covered in above categories.
    
    
    \param[in] env Scons environment.
    
    '''
    ret = ["-DMOREFEM_LLVM_CLANG"]
    
    if env['MODE'] == 'release':
        ret.append('-flto')

    return ret