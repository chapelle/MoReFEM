/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace TestVertexMatchingNS
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class Model;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        /*!
         * \brief Analyze the results of the test model and check the interpolator works as intended.
         *
         * \param[in] model The dummy \a Model used in current test.
         */
        void CheckInterpolator(const Model& model);


    } // namespace TestVertexMatchingNS


} // namespace MoReFEM


# include "Test/Operators/NonConformInterpolator/FromVertexMatching/CheckInterpolator.hxx"


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_CHECK_INTERPOLATOR_HPP_
