///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Apr 2015 12:13:39 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HPP_

# include <memory>
# include <vector>
# include <cassert>

# include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

# include "Core/NumberingSubset/NumberingSubset.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            class NdofHolder;


        } // namespace FEltSpaceNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Intermediate object used to create and store the pattern of a GlobalMatrix.
             *
             * This is mostly a facilitator of ThirdParty::Wrappers::Petsc::MatrixPattern that can create such an object
             * with ease due to the effectiveness of numbering subsets (which are not available at ThirdParty library level).
             */
            class MatrixPattern
            {

            public:

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const MatrixPattern>;

                //! Alias to vector of unique pointers.
                using vector_const_unique_ptr = std::vector<const_unique_ptr>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Computes the number of diagonal and off-diagonal terms for each row of a dof matrix.
                 *
                 *
                 * Beware: The indexes of the row are processor-wise, whereas the values for each vector are program-wise!
                 * (not my fault: it is the way Petsc is designed...)
                 *
                 * \param[in] connectivity For each node bearer, the list of connected node bearers. Computed by
                 * ComputeNodeBearerConnectivity().
                 * \param[in] processor_wise_node_bearer_list Node bearer list on the local processor. It is somewhat
                 * redundant with keys of \a connectivity; it is there only because it has the advantage of being already
                 * sort.
                 * \param[in] Ndof_holder Object which knows number of dofs in each numbering subset.
                 * \param[in] row_numbering_subset Numbering subset over which all dofs considered in the rows of the
                 * matrix are defined.
                 * \param[in] column_numbering_subset Numbering subset over which all dofs considered in the rows of the
                 * matrix are defined.
                 */
                explicit MatrixPattern(const NumberingSubset::const_shared_ptr& row_numbering_subset,
                                       const NumberingSubset::const_shared_ptr& column_numbering_subset,
                                       const connectivity_type& connectivity,
                                       const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                       const NdofHolder& Ndof_holder);

                //! Destructor.
                ~MatrixPattern() = default;

                //! Copy constructor.
                MatrixPattern(const MatrixPattern&) = delete;

                //! Move constructor.
                MatrixPattern(MatrixPattern&&) = delete;

                //! Copy affectation.
                MatrixPattern& operator=(const MatrixPattern&) = delete;

                //! Move affectation.
                MatrixPattern& operator=(MatrixPattern&&) = delete;

                ///@}

                //! Get the numbering subset used for rows.
                const NumberingSubset& GetRowNumberingSubset() const;

                //! Get the numbering subset used for columns.
                const NumberingSubset& GetColumnNumberingSubset() const;

                //! Get the pattern to feed to Petsc.
                const ::MoReFEM::Wrappers::Petsc::MatrixPattern& GetPattern() const;


            private:

                //! Numbering subset over which all dofs considered in the rows of the matrix are defined.
                const NumberingSubset::const_shared_ptr row_numbering_subset_ = nullptr;

                /*!
                 * \brief Numbering subset over which all dofs considered in the rows of the matrix are defined.
                 *
                 * Might be the same as \a row_numbering_subset_.
                 */
                const NumberingSubset::const_shared_ptr column_numbering_subset_ = nullptr;

                //! Objects that stores effectively the pattern.
                ::MoReFEM::Wrappers::Petsc::MatrixPattern::const_unique_ptr matrix_pattern_ = nullptr;



            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/MatrixPattern.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MATRIX_PATTERN_HPP_
