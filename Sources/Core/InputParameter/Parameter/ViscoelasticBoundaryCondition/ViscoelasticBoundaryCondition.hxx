///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 18 Apr 2016 11:44:26 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup CoreGroup
/// \addtogroup CoreGroup
/// \{

#ifndef MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HXX_
# define MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HXX_


namespace MoReFEM
{


    namespace InputParameter
    {



    } // namespace InputParameter


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_PARAMETER_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HXX_
