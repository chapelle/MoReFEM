class Target(object):
    """Object which handles the executable to compile and check and the Lua file to use in those tests."""
    
    def __init__(self, target_name, lua_file_list, do_call_ensight_output = True, Nproc = 4, is_sequential = True):
        """
        \param[in] target_name Name of the executable.
        \param[in] lua_file_list List of Lua files to run against the executable. The path to each of this file is given relatively to the root of MoReFEM directory.
        \param[in] do_call_ensight_output True if Ensight output is meaningfull for the target. Usually True for demo 
        ModelInstance and False for integration tests.
        \param[in] Nproc Number of processors to deal with in parallel. Put 0 or 1 if parallel is not relevant for the target.        
        \param[in] is_sequential For some tests related to parallel behaviour, sequential run is meaningless.
        """
        self.executable = target_name
        self.lua_file_list = lua_file_list
        self.do_call_ensight_output = do_call_ensight_output
        self.Nproc = Nproc
        self.is_sequential = is_sequential
        
        
class TestTarget(Target):
    
    def __init__(self, target_name, lua_file_list, Nproc = 4, is_sequential = True):    
    
        super(TestTarget, self).__init__(target_name,
                                         lua_file_list,
                                         False,
                                         Nproc,
                                         is_sequential)
        


def TestTargetList():
    """List of all tests to run in the integration process."""
    

    vertex_matching_operator_lua_list = \
('Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_scalar_P1.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P1.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P1b.lua', \
        'Sources/Test/Operators/NonConformInterpolator/FromVertexMatching/demo_input_test_from_vertex_matching_vectorial_P2.lua')

    # IMPORTANT:
    # test_mpi_error_handling is ignored for the time being, as it is a test designed to fail.
    # Add it properly in the upcoming continuous integration test!
    
    return (\
    TestTarget('test_input_parameter_data', \
           ('Sources/Test/Utilities/InputParameterList/demo_input_parameter_test_ipl.lua', )),
    TestTarget('test_mpi_functions', \
            ('Sources/Test/ThirdParty/Mpi/Functions/demo_input_parameter_mpi_functions.lua', ),
            is_sequential = False
            ),
    TestTarget('test_mpi_send_receive_array', \
            ('Sources/Test/ThirdParty/Mpi/SendReceive/demo_input_parameter_mpi_send_receive.lua', ),
            is_sequential = False
            ),            
    TestTarget('test_mpi_send_receive_single_value', \
            ('Sources/Test/ThirdParty/Mpi/SendReceive/demo_input_parameter_mpi_send_receive.lua', ),
            is_sequential = False
            ),              
    TestTarget('test_ondomatic', \
           ('Sources/Test/Ondomatic/demo_input_ondomatic_2D.lua',
            'Sources/Test/Ondomatic/demo_input_ondomatic_3D.lua')),
    TestTarget('test_domain_list_in_coords', \
           ('Sources/Test/Geometry/DomainListInCoords/demo_input_test_domain_list_in_coords.lua', )),
    TestTarget('test_lightweight_domain_list', \
           ('Sources/Test/Geometry/LightweightDomainList/demo_input_test_lightweight_domain_list.lua', )),
    TestTarget('test_coloring', \
           ('Sources/Test/Geometry/Coloring/demo_input_test_coloring.lua', )),
    TestTarget('test_coords_in_parallel', \
           ('Sources/Test/Geometry/CoordsInParallel/demo_input_test_coords_in_parallel.lua', ),
           is_sequential = False),
    TestTarget('test_movemesh', \
           ('Sources/Test/Geometry/Movemesh/demo_input_parameter_movemesh.lua', )),           
    TestTarget('test_p1_to_p2', \
           ('Sources/Test/Operators/P1_to_HigherOrder/demo_input_test_P1_to_P2.lua', )),
    TestTarget('test_p1_to_p1b', \
           ('Sources/Test/Operators/P1_to_HigherOrder/demo_input_test_P1_to_P1b.lua', )),
    TestTarget('test_conform_operator', \
           ('Sources/Test/Operators/ConformProjector/demo_input_test_conform_projector.lua', )),
    TestTarget('test_vertex_matching_operator', vertex_matching_operator_lua_list),
    # TestTarget('test_param_at_dof', \
    #        ('Sources/Test/Parameter/AtDof/demo_input_test_at_dof_parameter.lua', )),
    TestTarget('test_parameter_time_dep', \
           ('Sources/Test/Parameter/TimeDependency/demo_input_test_parameter_time_dependency.lua', ),
            Nproc = 0),
    TestTarget('operators_test_functions', \
           ('Sources/Test/Operators/TestFunctions/demo_input_parameter_test_functions_1D.lua', \
           'Sources/Test/Operators/TestFunctions/demo_input_parameter_test_functions_2D.lua', \
           'Sources/Test/Operators/TestFunctions/demo_input_parameter_test_functions_3D.lua'), \
           Nproc = 2),

    )
    
    return ret
    

def ModelTargetList():
    """List of all the demonstration model instances to run in the integration process."""
    
    return (\
    Target('stokes', \
          ('Sources/ModelInstances/Stokes/demo_input_stokes.lua', )), \
    Target('stokes_2_op', \
          ('Sources/ModelInstances/Stokes/demo_input_stokes_2_operators.lua', ),
          do_call_ensight_output = False), \
    Target('elasticity', \
          ('Sources/ModelInstances/Elasticity/demo_input_elasticity.lua', \
           'Sources/ModelInstances/Elasticity/demo_input_elasticity_3d.lua' )), \
    Target('midpoint_hyperelasticity', \
          ('Sources/ModelInstances/Hyperelasticity/demo_input_hyperelasticity.lua', )), \
    Target('heat', \
          ('Sources/ModelInstances/Heat/demo_input_heat.lua', \
           'Sources/ModelInstances/Heat/demo_input_heat_1d.lua')), \
    Target('rivlin_cube', \
          ('Sources/ModelInstances/RivlinCube/demo_input_rivlin_cube_hexahedra.lua', \
           'Sources/ModelInstances/RivlinCube/demo_input_rivlin_cube_tetrahedra.lua')), \
    Target('laplacian', \
          ('Sources/ModelInstances/Laplacian/demo_input_laplacian.lua', )), \

    )
    
    
    
def TestAndModelTargetList():
    """List of all the tests and all the demonstration model instances to run in the integration process."""
    
    ret = list(TestTargetList())
    ret.extend(ModelTargetList())
       
    return ret
    
   
    
