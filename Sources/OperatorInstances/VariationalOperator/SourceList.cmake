target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
)

include(${CMAKE_CURRENT_LIST_DIR}/BilinearForm/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/NonlinearForm/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LinearForm/SourceList.cmake)
