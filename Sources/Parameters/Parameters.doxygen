/*!
* \defgroup ParametersGroup Parameter
*
* \brief This module encompass the definition of so-called Parameters, which are objects for which values at quadrature
* points might be queried.
*
* There are two distinct categories of \a Parameter:
* - Those that are defined from the input parameter file. They might be constant, piece constant by domain or given
* by an analytical Lua function, at the choice of the user of the model.
* - Parameters defined at dofs (that extract values from an underlying \a GlobalVector) or directly at quadrature points,
* which are hardcoded in each model.
*/


/// \addtogroup ParametersGroup
///@{


/// \namespace MoReFEM::ParameterNS
/// \brief Namespace that enclose stuff related to \a Parameter.


///@} // addtogroup



/*!
 * \class doxygen_hide_parameter_domain_arg
 *
 * \param[in] domain \a Domain upon which the \a Parameter is defined.
 */


/*!
 * \class doxygen_hide_parameter_local_coords_type
 *
 * \brief Convenient alias to decide where a \a Parameter is computed.
 *
 * Parameters are by construct objects that are evaluated at a local position in a given \a GeometricElt. Depending
 * on the exact type of the \a Parameter, the local position might be given by either a \a LocalCoords or a \a
 * QuadraturePoint (the latter being a child of the former).
 */
