///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FormulationSolverGroup
/// \addtogroup FormulationSolverGroup
/// \{

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HPP_

# include <vector>

# include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"
# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"


# include "Utilities/Containers/UnorderedMap.hpp"

# include "Core/Enum.hpp"
# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"
# include "Core/TimeManager/TimeManager.hpp"
# include "Core/Solver/Solver.hpp"

# include "Geometry/Mesh/Internal/GeometricMeshRegionManager.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

# include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
# include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"

# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"

# include "Parameters/Parameter.hpp"

# include "Operators/Enum.hpp"

# include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp"
# include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp"
# include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
# include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"


namespace MoReFEM
{


    /// \addtogroup FormulationSolverGroup
    ///@{


    /*!
     * \brief Whether the matrix has already been factorized or not.
     *
     * If yes, there is a huge speed-up in the solver, but you must be sure the matrix is still the same.
     * So in the first call, the value must be 'no'.
     */
    enum class IsFactorized { yes, no };


    namespace VariationalFormulationNS
    {


        /*!
         * \brief Specify whether boundary conditions are applied on system rhs, system matrix or both.
         */
        enum class On
        {
            system_matrix,
            system_rhs,
            system_matrix_and_rhs
        };


    } // namespace VariationalFormulationNS


    /*!
     * \brief CRTP base for VariationalFormulation.
     *
     * This class is in charge of the bulk of the calculation: storage of global matrices and vectors, assembling,
     * call to the solver.
     *
     * \tparam SolverIndexT Index in the input parameter file of the solver to use.
     *
     * \tparam DerivedT Class which will inherit from this one through CRTP.
     * DerivedT is expected to define the following methods (otherwise the compilation will fail):
     * - void SupplInit(const InputParameterList& input_parameter_data) // where InputParameterList is defined within
     * the model instance.
     * - void AllocateMatricesAndVectors()
     * - Wrappers::Petsc::Snes::SNESFunction ImplementSnesFunction() const
     * - Wrappers::Petsc::Snes::SNESJacobian ImplementSnesJacobian() const
     * - Wrappers::Petsc::Snes::SNESViewer ImplementSnesViewer() const
     *
     * The ImplementSnesXXX() might return nullptr if you do not wish to allow non-linear solver for your problem.
     *
     */
    template
    <
        class DerivedT,
        unsigned int SolverIndexT
    >
    class VariationalFormulation
    : public Crtp::CrtpMpi<VariationalFormulation<DerivedT, SolverIndexT>>
    {


    protected:


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * The constructor by itself doesn't do much: most of the initialisation process occurs in the
         * subsequent call to Init().
         *
         * \internal <b><tt>[internal]</tt></b> The reason of this two-step approach is a constraint from C++ language:
         * initialisation calls methods defined in DerivedT through CRTP pattern, and this is illegal to
         * do so in a constructor.
         * This isn't much of an issue: VariationalFormulation is expected to be build solely within base Model
         * class, and this class knows this call must occur (it is already coded once and for all in its internals).
         *
         * \copydoc doxygen_hide_init_morefem_param
         * \copydetails doxygen_hide_time_manager_arg
         * \param[in] god_of_dof God of dof into which the formulation works.
         * \param[in] boundary_condition_list List of Dirichlet boundary conditions to take into account in the
         * variational formulation.
         */
        template<class MoReFEMDataT>
        VariationalFormulation(const MoReFEMDataT& morefem_data,
                               TimeManager& time_manager,
                               const GodOfDof& god_of_dof,
                               DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

        //! Destructor.
        ~VariationalFormulation() = default;

        //! Recopy constructor - deactivated.
        VariationalFormulation(const VariationalFormulation&) = delete;

        //! Move constructor - deactivated.
        VariationalFormulation(VariationalFormulation&&) = delete;

        //! Copy assignation.
        VariationalFormulation& operator=(const VariationalFormulation&) = delete;

        //! Move assignation.
        VariationalFormulation& operator=(VariationalFormulation&&) = delete;


        ///@}

    public:


        /*!
         * \brief Performs the complete build of the object.
         *
         * Must be called immediately after the constructor (but it is already taken care of - see constructor
         * help for more details).
         *
         * \copydoc doxygen_hide_init_morefem_param
         *
         */
        template<class MoReFEMDataT>
        void Init(const MoReFEMDataT& morefem_data);


    public:

        /*!
         * \class doxygen_hide_varf_solve_linear
         *
         * \brief Perform a linear solve, using Petsc's KSP algorithm.
         *
         * This method does only the resolution itself; assembling or applying boundary conditions must be
         * done beforehand.
         *
         * \tparam IsFactorizedT If the same matrix has already been set up previously you should rather
         * set this parameter to yes to avoid a call to Petsc's KSPSetOperators() and to reuse the already
         * computed factorized matrix. However, in doubt (acceptable during development process...), the safe
         * option is 'no'.
         */

        /*!
         *  \class doxygen_hide_varf_solve_numbering_subset_arg
         *
         * \param[in] row_numbering_subset Solver will be applied on the system matrix which row is given by this
         * numbering subset.
         * \param[in] col_numbering_subset Solver will be applied on the system matrix which column is given by this
         * numbering subset, and with the RHS pointed out by this very same numbering subset.
         */


        /*!
         * \copydoc doxygen_hide_varf_solve_linear
         *
         * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] do_print_solver_infos Whether the Petsc solver should print informations about the solve in
         * progress on standard output.
         */
        template<IsFactorized IsFactorizedT>
        void SolveLinear(const NumberingSubset& row_numbering_subset,
                         const NumberingSubset& col_numbering_subset,
                         const char* invoking_file, int invoking_line,
                         Wrappers::Petsc::print_solver_infos do_print_solver_infos = Wrappers::Petsc::print_solver_infos::yes);


        /*!
         * \copydoc doxygen_hide_varf_solve_linear
         *
         * This version is broader that the overload above, as you can give your own matrix and vector rather than
         * using the so-called 'system' ones.
         *
         * \param[in] matrix Matrix part of the equation to solve (e.g. A in 'A X = R').
         * \param[in] rhs Rhs part of the equation to solve (e.g. R in 'A X = R').
         * \param[out] out Solution part of the equation to solve (e.g. X in 'A X = R'). It must have been properly
         * allocated; only the content is filled.
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] do_print_solver_infos Whether the Petsc solver should print informations about the solve in
         * progress on standard output.
         *
         */
        template<IsFactorized IsFactorizedT>
        void SolveLinear(const GlobalMatrix& matrix,
                         const GlobalVector& rhs,
                         GlobalVector& out,
                         const char* invoking_file, int invoking_line,
                         Wrappers::Petsc::print_solver_infos do_print_solver_infos = Wrappers::Petsc::print_solver_infos::yes);


        /*!
         * \brief Perform a non-linear solve, using Petsc's Newton SNES algorithm.
         *
         * \internal <b><tt>[internal]</tt></b> This method requires that ImplementSnesFunction(), ImplementSnesJacobian()
         * and ImplementSnesViewer() all returns something that is not nullptr.
         *
         * As assembling and application of boundary conditions may occur at each Newton iteration, these
         * operations are often handled within the snes function given by ImplementSnesFunction(); so it differs
         * from SolveLinear() on that matter.
         *
         * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         * \copydoc doxygen_hide_invoking_file_and_line
         * \param[in] do_check_convergence Check the convergence if needed. If it does not converge throw an error.
         */
        void SolveNonLinear(const NumberingSubset& row_numbering_subset,
                            const NumberingSubset& col_numbering_subset,
                            const char* invoking_file, int invoking_line,
                            Wrappers::Petsc::check_convergence do_check_convergence =  Wrappers::Petsc::check_convergence::yes);

    public:


        /*!
         * \brief Apply essential boundary counditions.
         *
         * \internal <b><tt>[internal]</tt></b> Natural boundary conditions are managed by dedicated operators (their
         * 'naturalness' is fully used!)
         *
         * \tparam AppliedOnT Whether the essential boundary conditions are applied on matrix, vector or both.
         * \tparam BoundaryConditionMethod Choice of the method used to apply Dirichlet boundary conditions.
         *
         * * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         */
        template
        <
            VariationalFormulationNS::On AppliedOnT,
            BoundaryConditionMethod BoundaryConditionMethodT = BoundaryConditionMethod::pseudo_elimination
        >
        void ApplyEssentialBoundaryCondition(const NumberingSubset& row_numbering_subset,
                                             const NumberingSubset& col_numbering_subset);

        /*!
         * \brief Apply essential boundary counditions on a vector or a matrix which is not a system one.
         *
         *
         * \internal <b><tt>[internal]</tt></b> Natural boundary conditions are managed by dedicated operators (their
         * 'naturalness' is fully used!)
         *
         * \tparam BoundaryConditionMethod Choice of the method used to apply Dirichlet boundary conditions.
         * \tparam LinearAlgebraT Either \a GlobalMatrix or \a GlobalVector.
         *
         * \param[in] linear_algebra Vector or matrix to apply the boundary conditions on.
         *
         */
        template
        <
            class LinearAlgebraT,
            BoundaryConditionMethod BoundaryConditionMethodT = BoundaryConditionMethod::pseudo_elimination
        >
        void ApplyEssentialBoundaryCondition(LinearAlgebraT& linear_algebra);


        /*!
         * \brief Print min and max of the solution of the system solved (should be removed at some point).
         *
         * Works only when one unknown involved in the model.
         *
         * \param[in] numbering_subset Numbering subset onto which solution is described.
         * \param[in] verbose Arbitrary value that decides what is printed or not. Thresholds are currently applied
         * at 9 and 19; this is really a remnant of the Felisce era.
         */
        void DebugPrintSolutionElasticWithOneUnknown(const NumberingSubset& numbering_subset,
                                                     unsigned int verbose = 0) const;

        /*!
         * \brief Print min and max of the solution of the system solved (should be removed at some point).
         *
         * \param[in] numbering_subset Numbering subset onto which solution is described.
         */
        void DebugPrintSolution(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Write the solution in the output directory given in the input parameter file.
         *
         * \copydetails doxygen_hide_time_manager_arg
         * \param[in] numbering_subset Numbering subset onto which solution is described.
         */
        void WriteSolution(const TimeManager& time_manager,
                           const NumberingSubset& numbering_subset) const;


        /*!
         * \brief Print norm of the solution of the system solved (should be removed at some point).
         *
         * \param[in] numbering_subset Numbering subset onto which solution is described.
         *
         * SHould be removed at some point, especially considering proper tools to compute the norm based directly
         * on Petsc have been introduced since.
         */
        void DebugPrintNorm(const NumberingSubset& numbering_subset) const;

    public:


        /*!
         * \class doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * \param[in] row_numbering_subset Matrix required is the one which row is described by this numbering subset.
         * \param[in] col_numbering_subset Matrix required is the one which column is described by this numbering subset.
         *
         */

        /*!
         * \class doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * \return Reference to the adequate system matrix.
         */


        /*!
         * \brief Access to the system matrix (A in A X = R).
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         */
        const GlobalMatrix& GetSystemMatrix(const NumberingSubset& row_numbering_subset,
                                            const NumberingSubset& col_numbering_subset) const;

        /*!
         * \brief Non constant access to the system matrix (A in A X = R).
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         */
        GlobalMatrix& GetNonCstSystemMatrix(const NumberingSubset& row_numbering_subset,
                                            const NumberingSubset& col_numbering_subset);


        /*!
         * \class doxygen_hide_varf_vector_numbering_subset
         *
         * \param[in] numbering_subset Vector required is the one described by this numbering subset.
         *
         */

        /*!
         * \class doxygen_hide_varf_vector_numbering_subset_and_return
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset
         *
         * \return Reference to the required vector.
         */



        /*!
         * \brief Access to the system rhs (R in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        const GlobalVector& GetSystemRhs(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Non constant access to the system rhs (R in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        GlobalVector& GetNonCstSystemRhs(const NumberingSubset& numbering_subset);

        /*!
         * \brief Access to the system solution (X in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        const GlobalVector& GetSystemSolution(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Non constant access to the system solution (X in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        GlobalVector& GetNonCstSystemSolution(const NumberingSubset& numbering_subset);

        //! \copydoc doxygen_hide_time_manager_accessor
        const TimeManager& GetTimeManager() const;


    public:
        
        /*!
         * \brief Get the directory into which all outputs are written.
         *
         * It is assumed to already exist when this call is made.
         *
         * \return Directory into which all outputs are written.
         */
        const std::string& GetResultDirectory() const noexcept;

        /*!
         * \brief Returns the output directory into which solutions for \a numbering_subset are written.
         *
         * \param[in] numbering_subset \a NumberingSubset of the solution for which the output directory is requested.
         *
         * \return Path to the output directory into which solutions related to \a numbering_subset are written.
         */
        std::string GetOutputDirectory(const NumberingSubset& numbering_subset) const;



    protected:


        /*!
         * \brief Allocate the global matrix circonscribed by the two given numbering subsets.
         *
         * This method is merely a wrapper over MoReFEM::AllocateGlobalMatrix() specialized for the system matrices
         * within the \a VariationalFormulation.
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * This is done once and for all during the initialisation phase of MoReFEM; no others should be
         * allocated in the core of the calculation.
         *
         * The data have already been reduced to processor-wise when this operation is performed (or for that
         * matter when VariationalFormulation are expected to be built, i.e. typically in
         * the method \a SupplInitialize() to define in each model instance).
         *
         * The pattern of the matrix is expected to have been computed in the GodOfDof before the reduction of data.
         *
         */
        void AllocateSystemMatrix(const NumberingSubset& row_numbering_subset,
                                  const NumberingSubset& col_numbering_subset);


        /*!
         * \brief Allocate the global vector circonscribed by the given numbering subset.
         *
         * This method is merely a wrapper over MoReFEM::AllocateGlobalVector() specialized for the system vectors
         * within the \a VariationalFormulation.
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset
         *
         * This is done once and for all during the initialisation phase of MoReFEM; no others should be
         * allocated in the core of the calculation.
         *
         * The data have already been reduced to processor-wise when this operation is performed (or for that
         * matter when VariationalFormulation are expected to be built, i.e. typically in method SupplInitialize()
         * of the model).
         *
         * \internal <b><tt>[internal]</tt></b> Currently both rhs and solutions are allocated by this call.
         *
         */
        void AllocateSystemVector(const NumberingSubset& numbering_subset);

        /*!
         * \class doxygen_hide_init_vector_system_solution
         *
         * \brief Initialize the vector system solution with an initial condition from the input file with respect to
         * the unknown considered.
         *
         * \todo #859 Couldn't this functionality be fulfilled with a source operator?
         *
         * \tparam IndexT Input parameter that gives away the relevant informations is InputParameter::InitialCondition<IndexT>.
         * \tparam InputParameterDataT Type of the structure that holds the model settings.
         * \copydoc doxygen_hide_input_parameter_data_arg
         * \param[in] numbering_subset \a NumberingSubset related to the \a unknown in the \a felt_space.
         * \param[in] felt_space \a FEltSpace onto which the vector is to be initialized.
         * \param[in] unknown \a Unknown considered in the vector.
         *
         */

        //! \copydoc doxygen_hide_init_vector_system_solution
        template<unsigned int IndexT, class InputParameterDataT>
        void InitializeVectorSystemSolution(const InputParameterDataT& input_parameter_data,
                                            const NumberingSubset& numbering_subset,
                                            const Unknown& unknown,
                                            const FEltSpace& felt_space);


        /*!
         * \copydoc doxygen_hide_init_vector_system_solution
         *
         * \param[in,out] vector Vector which value must be initialized. This vector must already been properly allocated,
         * and itss associated numbering subset must match \a numbering_subset.
         *
         * This overload works on a \a vector given in arguments rather than on the default system vector.
         *
         */
        template <unsigned int IndexT, class InputParameterDataT>
        void InitializeVectorSystemSolution(const InputParameterDataT& input_parameter_data,
                                            const NumberingSubset& numbering_subset,
                                            const Unknown& unknown,
                                            const FEltSpace& felt_space,
                                            GlobalVector& vector);

    public:

        //! List of essential boundary condition to consider.
        const DirichletBoundaryCondition::vector_shared_ptr& GetEssentialBoundaryConditionList() const noexcept;

    public:

        //! Get god of dof.
        const GodOfDof& GetGodOfDof() const noexcept;

    protected:

        //! Access to the Snes underlying object.
        const Wrappers::Petsc::Snes& GetSnes() const noexcept;

    // private: // \todo #1034 SHould be private once ticket solved (arguably constant version should also be...)

        //! Non constant access to the Snes underlying object.
        Wrappers::Petsc::Snes& GetNonCstSnes() noexcept;

    private:

        /*!
         * \brief Access to the class in charge of storing the global matrices for each relevant
         * combination of numbering subsets.
         */
        const Internal::VarfNS::GlobalMatrixStorage& GetGlobalMatrixStorage() const;


        /*!
         * \brief Non constant access to the class in charge of storing the global matrices for each relevant
         * combination of numbering subsets.
         */
        Internal::VarfNS::GlobalMatrixStorage& GetNonCstGlobalMatrixStorage();

        /*!
         * \brief Access to the class in charge of storing the rhs vectors for each relevant numbering subset.
         */
        const Internal::VarfNS::GlobalVectorStorage& GetRhsVectorStorage() const;


        /*!
         * \brief Access to the class in charge of storing the rhs vectors for each relevant numbering subset.
         */
        Internal::VarfNS::GlobalVectorStorage& GetNonCstRhsVectorStorage();


        /*!
         * \brief Access to the class in charge of storing the solution vectors for each relevant numbering subset.
         */
        const Internal::VarfNS::GlobalVectorStorage& GetSolutionVectorStorage() const;


        /*!
         * \brief Access to the class in charge of storing the solution vectors for each relevant numbering subset.
         */
        Internal::VarfNS::GlobalVectorStorage& GetNonCstSolutionVectorStorage();

    protected:

        //! Files will be written every \a GetDisplayValue() time iterations.
        unsigned int GetDisplayValue() const;

    protected:

        //! \copydoc doxygen_hide_time_manager_accessor
        TimeManager& GetNonCstTimeManager();

    private:

        /// \name Global matrices and vectors.


        ///@{

        /*!
         * \brief List of system matrices.
         *
         */
        Internal::VarfNS::GlobalMatrixStorage global_matrix_storage_;

        //! System rhs.
        Internal::VarfNS::GlobalVectorStorage global_rhs_storage_;

        //! System solution.
        Internal::VarfNS::GlobalVectorStorage global_solution_storage_;


        ///@}

        //! Directory into which all outputs are written.
        const std::string& result_directory_;

        //! Felisce transient parameters.
        TimeManager& time_manager_;

        /*!
         * \brief Wrapper over Petsc solver utilities.
         *
         * \internal <b><tt>[internal]</tt></b> For conveniency same class is used for all cases (including mere KSP solve).
         */
        Wrappers::Petsc::Snes::unique_ptr snes_ = nullptr;


        //! God of dof.
        const GodOfDof& god_of_dof_;

        //! List of essential boundary condition to consider.
        const DirichletBoundaryCondition::vector_shared_ptr boundary_condition_list_;

        //! Display value.
        unsigned int display_value_;

    };


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/VariationalFormulation.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_VARIATIONAL_FORMULATION_HPP_
