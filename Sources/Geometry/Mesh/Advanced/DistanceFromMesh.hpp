///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 17 Jun 2016 16:47:49 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_DISTANCE_FROM_MESH_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_DISTANCE_FROM_MESH_HPP_

# include <memory>
# include <vector>

# include "Geometry/Coords/Coords.hpp"
# include "Utilities/Containers/EnumClass.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricMeshRegion;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace MeshNS
        {


            /*!
             * \brief Class in charge to compute the distance between a point and a mesh.
             *
             */
            class DistanceFromMesh final
            {

            public:

                //! Alias over shared_ptr.
                using unique_ptr = std::unique_ptr<DistanceFromMesh>;

            public:


                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 */
                explicit DistanceFromMesh();

                //! Destructor.
                ~DistanceFromMesh() = default;

                //! Do not allow recopy.
                DistanceFromMesh(const DistanceFromMesh&) = delete;

                //! Do not allow move.
                DistanceFromMesh(DistanceFromMesh&&) = delete;

                //! Do not allow copy affectation.
                DistanceFromMesh& operator=(const DistanceFromMesh&) = delete;

                //! Do not allow move affectation.
                DistanceFromMesh& operator=(DistanceFromMesh&&) = delete;


                ///@}

                /*!
                 * \brief Computes the distance between a point and a given mesh.
                 *
                 * \param[in] point Point to compute the distance.
                 * \param[in] mesh Surface definition with Triangle3, Edges and Points.
                 * \param[in] point_projection Projection of the point on the mesh.
                 * \param[in] normal_on_point_projection Normal to the mesh at the projection point.
                 *
                 * \return Distance between a point and a given mesh.
                 */
                double ComputeDistance(const SpatialPoint& point,
                                       const GeometricMeshRegion& mesh,
                                       SpatialPoint& point_projection,
                                       SpatialPoint& normal_on_point_projection);

            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================

            private:

                /*!
                 * \brief Internal functions to determine the region.
                 *
                 * \param[in] point_on_plane Projectted point ont the triangle plane.
                 * \param[in] point1 First point of the triangle.
                 * \param[in] point2 Second point of the triangle.
                 * \param[in] point3 Third point of the triangle.
                 * \param[in] k Case.
                 * \param[in] region Region of the triangle (triangle cut in 7 regions).
                 * \param[in] point_close Closest point of the triangle of the projection point.
                 *
                 * \return region of the triangle.
                 */
                unsigned int DistanceFromTriangleCaseK(const SpatialPoint& point_on_plane,
                                                       const SpatialPoint& point1,
                                                       const SpatialPoint& point2,
                                                       const SpatialPoint& point3,
                                                       const unsigned int k,
                                                       unsigned int region,
                                                       SpatialPoint& point_close);

            private:

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointOnPlane() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointOnPlane() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointOnPlanePoint() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointOnPlanePoint() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointClose() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointClose() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointPoint1() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointPoint1() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint1Point() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint1Point() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint1Point2() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint1Point2() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint1Point3() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint1Point3() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint1PointOnPlane() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint1PointOnPlane() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint3Proj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint3Proj() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointProj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointProj() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint3Point3Proj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint3Point3Proj() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPointPointProj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPointPointProj() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint1PointProj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint1PointProj() noexcept;

                //! Constant accessor to the internal coord to help compute the distance.
                const SpatialPoint& GetPoint2PointProj() const noexcept;

                //! Non constant accessor to the internal coord to help compute the distance.
                SpatialPoint& GetNonCstPoint2PointProj() noexcept;

            private:

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_on_plane_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_on_plane_point_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_close_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_point1_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point1_point2_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point1_point3_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point1_point_on_plane_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point3_proj_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_proj_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point3_point3_proj_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point_point_proj_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point1_point_proj_ = nullptr;

                //! Internal coord to help compute the distance.
                SpatialPoint::unique_ptr point2_point_proj_ = nullptr;

                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================
            };


        } // namespace MeshNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Advanced/DistanceFromMesh.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_DISTANCE_FROM_MESH_HPP_
