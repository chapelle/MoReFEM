/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/Geometry/PseudoNormals.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"
# include "Core/InputParameter/Parameter/Source/VectorialTransientSource.hpp"
# include "Core/InputParameter/Parameter/Solid/Solid.hpp"
# include "Core/InputParameter/Parameter/Source/Pressure.hpp"
# include "Core/InputParameter/Solver/Petsc.hpp"
# include "Core/InputParameter/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"


namespace MoReFEM
{


    namespace MidpointHyperelasticityNS
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            displacement = 1
        };

        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            displacement = 1
        };

        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            full_mesh = 1,
            volume = 2,
            force = 3,
            dirichlet = 4,
        };

        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            clamped = 1
        };

        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            volume = 1,
            force = 2
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic = 1
        };

        //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
        <
            InputParameter::TimeManager,

            InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,

            InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

            InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputParameter::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::force)>,
            InputParameter::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputParameter::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::clamped)>,

            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>,
            InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>,

            InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputParameter::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>,

            InputParameter::Solid::VolumicMass,
            InputParameter::Solid::HyperelasticBulk,
            InputParameter::Solid::Kappa1,
            InputParameter::Solid::Kappa2,

            InputParameter::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


    } // namespace CardiacMechanicsPrestressNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HYPERELASTICITY_x_INPUT_PARAMETER_LIST_HPP_
