///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 17:15:29 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#include <iostream>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Miscellaneous.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace RefFEltNS
        {
            
            
            BasicRefFElt::BasicRefFElt()
            : topology_dimension_(NumericNS::UninitializedIndex<decltype(topology_dimension_)>())
            { }
            
            
            BasicRefFElt::~BasicRefFElt() = default;
            
            
            void BasicRefFElt::Print(std::ostream& out) const
            {
                const unsigned int Nvertex = this->NnodeOnVertex();
                const unsigned int Nedge = this->NnodeOnEdge();
                const unsigned int Nface = this->NnodeOnFace();
                
                
                for (unsigned int i = 0u; i < Nvertex; ++i)
                {
                    auto vertex_dof_ptr = GetLocalNodeOnVertexPtr(i);
                    
                    if (!vertex_dof_ptr)
                        continue;
                    
                    out << "Vertex " << i << ": (" << vertex_dof_ptr->GetIndex() << ')' << std::endl;
                }
                
                
                if (AreNodesOnEdges())
                {
                    for (unsigned int i = 0u; i < Nedge; ++i)
                    {
                        for (unsigned int orientation = 0u; orientation < 2u; ++orientation)
                        {
                            auto dof_on_current_edge_list = GetLocalNodeOnEdgeList(i, orientation);
                            
                            if (dof_on_current_edge_list.empty())
                                continue;
                            
                            std::vector<unsigned int> indexes;
                            
                            for (const auto& local_node_ptr : dof_on_current_edge_list)
                            {
                                assert(!(!local_node_ptr));
                                indexes.push_back(local_node_ptr->GetIndex());
                            }
                            
                            out << "Edge " << i << ", Orientation " << orientation << ": ";
                            Utilities::PrintContainer(indexes, out, ",", "(", ")\n");
                        }
                    }
                }
                
                
                if (AreNodesOnFaces())
                {
                    for (unsigned int i = 0u; i < Nface; ++i)
                    {
                        for (unsigned int orientation = 0u; orientation < 8u; ++orientation)
                        {
                            auto dof_on_current_face_list = GetLocalNodeOnFaceList(i, orientation);
                            
                            if (dof_on_current_face_list.empty())
                                continue;
                            
                            std::vector<unsigned int> indexes;
                            
                            for (auto local_node_ptr : dof_on_current_face_list)
                            {
                                assert(!(!local_node_ptr));
                                indexes.push_back(local_node_ptr->GetIndex());
                            }
                            
                            out << "Face " << i << ", Orientation " << orientation << ": ";
                            Utilities::PrintContainer(indexes, out, ",", "(", ")\n");
                        }
                    }
                }
                
                
                if (AreNodesOnVolume())
                {
                    const auto& local_node_list_on_current_volume = GetLocalNodeOnVolumeList();
                    
                    std::vector<unsigned int> indexes;
                    
                    for (auto local_node_ptr : local_node_list_on_current_volume)
                    {
                        assert(!(!local_node_ptr));
                        indexes.push_back(local_node_ptr->GetIndex());
                    }
                    
                    out << "Volume: ";
                    Utilities::PrintContainer(indexes, out, ",", "(", ")\n");
                }
            }
            
            
            
            #ifndef NDEBUG
            
            
            //! Local function to check whether the list are well-formed.
            void CheckLocalNodeListConsistency(const LocalNode::vector_const_shared_ptr& local_node_list,
                                               InterfaceNS::Nature nature)
            {
                for (const auto& node_ptr : local_node_list)
                {
                    assert(!(!node_ptr));
                    assert(node_ptr->GetLocalInterface().GetNature() == nature);
                }
            }
            
            #endif // NDEBUG
            
            
        } // namespace RefFEltNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
