///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#include <array>
#include <algorithm>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/InputParameterList/Internal/ManualParsing/ManualParsing.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace InputParameterListNS
        {
            
            
            namespace // anonymous
            {
                
                
                /*!
                 * \brief Just concatenate the section, subsections and so on into a string.
                 *
                 * e.g. if hierarchy is 'Solid', 'YoungModulus' it will yield Solid.YoungModulus ('.' is the Ops
                 * separator).
                 */
                std::string GenerateSectionNameFromHierarchy(const std::vector<std::string>& hierarchy);
                
                
            } // namespace anonymous
            
                
            
            EntriesSortPerSection ExtractKeysFromFile(const std::string& path)
            {
                std::ifstream stream;
                FilesystemNS::File::Read(stream, path, __FILE__, __LINE__);
                
                auto npos = std::string::npos;
                
                std::string line;
                std::string variable;
                std::string lhs;
                
                std::vector<std::string> section_hierarchy;
                
                EntriesSortPerSection ret;
                ret.max_load_factor(Utilities::DefaultMaxLoadFactor());
                
                // All strings that might be tagged as variable or section and begins with one
                // of these keys followed by ' ' or '(' must be rejected.
                std::array<std::string, 3> rejected_keys
                {
                    {
                        "if",
                        "else",
                        "elseif"
                    }
                };
                
                unsigned int line_count = 0;
                
                
                while (std::getline(stream, line))
                {
                    ++line_count;
                    
                    // Delete the comment part of the line.
                    auto pos = line.find("--");
                    
                    if (pos != npos)
                        line.erase(pos, npos);
                    
                    // If there are more closing braces than opening ones on the line, it means the current
                    // section has been closed.
                    auto Nopening_brace = (std::count(line.cbegin(), line.cend(), '{'));
                    auto Nclosing_brace = (std::count(line.cbegin(), line.cend(), '}'));
                    
                    
                    
                    if (Nclosing_brace > Nopening_brace)
                    {
                        if (Nclosing_brace - Nopening_brace != 1u || section_hierarchy.empty())
                            throw Exception("Invalid Lua file: issue with closing braces somewhere in the block closed "
                                            "in line " + std::to_string(line_count) + " (might be too much of them, "
                                            ", missing commas or opening brace not on the same line as its '=' sign).",
                                            __FILE__,
                                            __LINE__);
                        
                        section_hierarchy.pop_back();
                    }
                    
                    // Consider only lines with an '='.
                    pos = line.find('=');
                    
                    if (pos == npos)
                        continue;
                    
                    // Extract the name of the variable (or of the section).
                    {
                        lhs = line.substr(0, pos);
                        Utilities::String::Strip(lhs, "\t\n ");
                        
                        // If there is if or else or elseif in it, it should be rejected.
                        bool reject_key = false;
                        
                        for (auto rejected_key : rejected_keys)
                        {
                            if (Utilities::String::StartsWith(lhs, rejected_key + "(") ||
                                Utilities::String::StartsWith(lhs, rejected_key + " "))
                            {
                                reject_key = true;
                                break;
                            }
                        }
                        
                        if (reject_key)
                            continue;
                    }
                    
                    
                    // Extract the associated value. If '{', it is a section, otherwise it is a mere variable.
                    if (Nopening_brace > Nclosing_brace)
                    {
                        if (Nopening_brace != Nclosing_brace + 1u)
                            throw Exception("Invalid Lua file: too much opening braces on line " + std::to_string(line_count),
                                            __FILE__,
                                            __LINE__);
                        
                        section_hierarchy.push_back(lhs);
                    }
                    else
                    {
                        variable = lhs;
                        ret[GenerateSectionNameFromHierarchy(section_hierarchy)].push_back(variable);
                    }
                }
                
                return ret;
            }
            
            
            namespace // anonymous
            {
                
                
                std::string GenerateSectionNameFromHierarchy(const std::vector<std::string>& hierarchy)
                {
                    std::string ret;
                    
                    for (const auto& item : hierarchy)
                        ret += item + ".";
                    
                    if (!ret.empty())
                        ret.resize(ret.size() - 1ul);

                    return ret;
                }
                
                
            } // namespace anonymous

                
        } // namespace InputParameterListNS
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
