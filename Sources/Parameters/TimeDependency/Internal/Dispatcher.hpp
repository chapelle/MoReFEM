///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 31 Mar 2016 15:56:23 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParametersGroup
/// \addtogroup ParametersGroup
/// \{

#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HPP_
# define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HPP_

# include <memory>
# include <vector>

# include "Parameters/ParameterType.hpp"

# include "Parameters/TimeDependency/None.hpp"
# include "Parameters/Internal/Alias.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            /*!
             * \brief Helper struct to statically dispatch some operations depending on whether a time dependency
             * is involved or not.
             *
             * I document here the generic case, but wil describe expected behaviour of the specialization
             * TimeDependencyNS::None for each method.
             *
             * \tparam TimeDependencyT An instantiation of \a TimeDependencyNS::Base class, which handles the
             * computation and the application of the time factor.
             * \copydoc doxygen_hide_tparam_parameter_type
             *
             *
             */
            template
            <
                ::MoReFEM::ParameterNS::Type TypeT,
                template<::MoReFEM::ParameterNS::Type> class TimeDependencyT
            >
            struct StaticIf
            {

                /*!
                 * \brief If TimeDependencyT == TimeDependencyNS::None, instantiate a dummy object \a time_dependency
                 * in Parameter class.
                 *
                 * Nothing is done if there is a time dependency (in which case user MUST explicitly call
                 * Parameter::SetTimeDependency()
                 *
                 * \param[out] time_dependency Reference to the namesake data attribute in Parameter class
                 */
                static void InitNoTimeDependencyHelper(typename TimeDependencyT<TypeT>::unique_ptr& time_dependency);

                /*!
                 * Return type used within Parameter class.
                 *
                 * For TypeT == vector or matrix, this is actually a const ref (e.g. const LocalVector&).
                 */
                using return_type = typename ::MoReFEM::ParameterNS::Traits<TypeT>::return_type;

                /*!
                 * \brief Apply the time factor, and hence return the full value of the Parameter at local position
                 * for current time step.
                 *
                 * \param[in] value_without_time_contribution Value of the spatial-only contribution.
                 * \param[in] time_dependency_object Object in charge of the time-only contribution.
                 * \return Full value of the Parameter at the chosen local position for current time step.
                 */
                static return_type ApplyTimeFactor(return_type value_without_time_contribution,
                                                   const TimeDependencyT<TypeT>& time_dependency_object);


            };



            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================

            template
            <
                ::MoReFEM::ParameterNS::Type TypeT
            >
            struct StaticIf<TypeT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>
            {

                using none_type = ::MoReFEM::ParameterNS::TimeDependencyNS::None<TypeT>;

                static void InitNoTimeDependencyHelper(typename none_type::unique_ptr& time_dependency);

                using return_type = typename ::MoReFEM::ParameterNS::Traits<TypeT>::return_type;

                static return_type ApplyTimeFactor(return_type value_without_time_contribution,
                                                   const none_type& time_dependency_object);


            };
            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/TimeDependency/Internal/Dispatcher.hxx"


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_INTERNAL_x_DISPATCHER_HPP_
