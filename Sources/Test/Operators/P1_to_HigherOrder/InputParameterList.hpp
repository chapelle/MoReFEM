/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_PARAMETER_LIST_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_PARAMETER_LIST_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputParameterData/InputParameterList.hpp"
# include "Core/InputParameter/Geometry/Domain.hpp"
# include "Core/InputParameter/FElt/FEltSpace.hpp"
# include "Core/InputParameter/FElt/Unknown.hpp"
# include "Core/InputParameter/FElt/NumberingSubset.hpp"



namespace MoReFEM
{


    namespace TestNS
    {


        namespace P1_to_P_HigherOrder_NS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                domain = 1
            };


            //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
            {
                fluid_velocity = 1,
                solid_velocity = 2
            };


            //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
            {
                velocity = 1,
                higher_order_velocity = 2,
                pressure= 3,
            };


            //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

            {
                solver = 1
            };


            //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
            {
                fluid_velocity = 1,
                solid_velocity = 2
            };



            //! \copydoc doxygen_hide_input_parameter_tuple
        using InputParameterTuple = std::tuple
            <
                InputParameter::TimeManager,

                InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fluid_velocity)>,
                InputParameter::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::solid_velocity)>,

                InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
                InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,
                InputParameter::Unknown<EnumUnderlyingType(UnknownIndex::higher_order_velocity)>,

                InputParameter::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputParameter::Domain<EnumUnderlyingType(DomainIndex::domain)>,

                InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid_velocity)>,
                InputParameter::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid_velocity)>,

                InputParameter::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

                InputParameter::Result
            >;


            //! \copydoc doxygen_hide_model_specific_input_parameter_list
        using InputParameterList = InputParameterList<InputParameterTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputParameterList>;


        } // namespace P1_to_P_HigherOrder_NS


    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_P1_xTO_x_HIGHER_ORDER_x_INPUT_PARAMETER_LIST_HPP_
