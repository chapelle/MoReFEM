import os
import SCons


def PathOrNone(key, val, env): 
    """For BLAS_LIB_DIR: None is also an acceptable value (in OS X as framework Accelerate is used)."""

    if val:
        return SCons.Variables.PathVariable.PathIsDir(key, val, env)

def ReadConfigurationFile(configuration_file):
    """Read the configuration file that details the settings specific to each user.
    
    
    """
    assert os.path.isfile(configuration_file)
    
    vars = SCons.Variables.Variables(configuration_file)
    vars.Add(SCons.Variables.EnumVariable('MODE', 'Whether you compile in debug or release mode. A hybrid mode for callgrind is also there (it is a release with debug symbols).', 'debug',
                          allowed_values=('debug', 'release', 'callgrind')))
    vars.Add(SCons.Variables.EnumVariable('LIBRARY_TYPE', 'Whether you want to use static or shared libraries.', None,
                          allowed_values=('static', 'shared', )))
                          
    vars.Add(SCons.Variables.BoolVariable('MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE',  \
                                          "If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.", \
                                          None))                          

    vars.Add(SCons.Variables.BoolVariable('MOREFEM_EXTENDED_TIME_KEEP',  \
                                          "If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored.", \
                                          None))  
                                          
    vars.Add(SCons.Variables.BoolVariable('MOREFEM_CHECK_NAN_AND_INF',  \
                                          "If true, there are additional checks that no nan and inf appears in the code. Even if False, solver always check for the validity of its solution (if a nan or an inf is present the SolveLinear() or SolveNonLinear() operation throws with a dedicated Petsc error). Advised in debug mode and up to you in release mode.", \
                                          None))                                                                    

    vars.Add(SCons.Variables.PathVariable("BUILD_DIR", "Path to the folder into which build will be installed.", None, SCons.Variables.PathVariable.PathIsDirCreate))
    vars.Add(SCons.Variables.PathVariable("INTERMEDIATE_BUILD_DIR", "Path to the folder into which intermediate build will be installed.", None, SCons.Variables.PathVariable.PathIsDirCreate))
                
    vars.Add('COMPILER', "Name of the compiler family used (clang or gcc currently)", None) 
    vars.Add('COMPILER_DIRECTORY', "Subdirectory(ies) to use within BUID_DIR and INTERMEDIATE_BUILD_DIR for the compiler. It is introduced to allow several directories for a same compiler: for instance if you want to build both gcc5 and gcc 6 you must provide 'gcc' in COMPILER but might name the directories 'gcc-5' and 'gcc-6'. If you do not need such refinement just use the same content as COMPILER field.", None) 
    vars.Add('CC', "C compiler", None) 
    vars.Add('CXX', "C++ compiler", None)

    vars.Add('CCC_CC', "Option used only for clang-analyzer; points to C compiler.", "clang") 
    vars.Add('CCC_CXX', "Option used only for clang-analyzer; points to C++ compiler.", "clang++")    
    vars.Add('CCC_ANALYZER_VERBOSE', "Verbose mode for clang-analyzer", 1)

    vars.Add(SCons.Variables.PathVariable('OPEN_MPI_INCL_DIR', "Includes of OpenMPI library.", None))


    vars.Add(SCons.Variables.BoolVariable('BLAS_CUSTOM_LINKER', "True if Blas is not handled as a standard library (typically for macOS/framework Accelerate).", 0))
    vars.Add(SCons.Variables.PathVariable('BLAS_LIB_DIR', "Directory in which Blas library may be found. Might be 'None'.", None, PathOrNone))
    vars.Add('BLAS_LIB', "Call to the linker to the library (e.g. '-framework Accelerate', '-lopenblas', ...).", None)

    vars.Add(SCons.Variables.PathVariable('PETSC_GENERAL_INCL_DIR', "Includes of Petsc library that are independant of the build used.", None))
    vars.Add(SCons.Variables.PathVariable('PETSC_DEBUG_INCL_DIR', "Includes of Petsc library related to debug build.", None))
    vars.Add(SCons.Variables.PathVariable('PETSC_RELEASE_INCL_DIR', "Includes of Petsc library related to release build.", None))
    vars.Add(SCons.Variables.PathVariable('PARMETIS_INCL_DIR', "Includes of Parmetis library.", None))
    vars.Add(SCons.Variables.PathVariable('LUA_INCL_DIR', "Includes of Lua library.", None))
    vars.Add(SCons.Variables.PathVariable('BOOST_INCL_DIR', "Includes of Boost library.", None))

    vars.Add(SCons.Variables.PathVariable('OPEN_MPI_LIB_DIR', "Path to OpenMPI library", None))
    vars.Add(SCons.Variables.PathVariable('PETSC_DEBUG_LIB_DIR', "Path to Petsc library built in debug mode.", None))
    vars.Add(SCons.Variables.PathVariable('PETSC_RELEASE_LIB_DIR', "Path to Petsc library built in release mode.", None))
    vars.Add(SCons.Variables.PathVariable('PARMETIS_LIB_DIR', "Path to Parmetis library.", None))
    vars.Add(SCons.Variables.PathVariable('LUA_LIB_DIR', "Path to Lua library.", None))
    vars.Add(SCons.Variables.PathVariable('BOOST_LIB_DIR', "Path to Boost library.", None))

    vars.Add(SCons.Variables.BoolVariable('PHILLIPS_LINKER', "True if you want to link with the Phillips Library. Only works with gcc.", 0))
    vars.Add(SCons.Variables.PathVariable('PHILLIPS_DIR', "Path to Phillips library and header. Might be 'None' if irrelevant", None, PathOrNone))
    
    return vars
    
    
