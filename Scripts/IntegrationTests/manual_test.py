import os
import sys
import subprocess
import multiprocessing
import datetime
import six

import targets
import CompareDirectories



def ReadConfigurationFile(myfile):
    """Read the configuration file, and load the few values that are relevant to tag the case considered in test.
    
    This is a temporary function: this should be reunited with the similar function used in SCons build.
    """
    
    field_list = {'COMPILER':None, 'COMPILER_DIRECTORY':None, 'MODE':None, 'LIBRARY_TYPE':None, 'BUILD_DIR':None, 'OPEN_MPI_INCL_DIR':None}
    
    with open(myfile, 'r') as f:
        for line in f:

            comment_sign = line.find('#')
            
            if comment_sign >= 0:
                line = line[:comment_sign]
                
            line = line.strip(" \'\n")
            
            if not line:
                continue
            
            splitted = line.split('=')
            
            assert(len(splitted) == 2) # Relevant lines should contain one and only one equal sign.
            
            value_read = splitted[1]
            value_read = value_read.strip(" \'")
            
            if not value_read:
                continue
            
            key_read = splitted[0].strip()
            
            for key,value in six.iteritems(field_list):
                if key_read == key:
                    
                    if value is not None:
                        raise Exception("Value for {0} in file {1} should be assigned only once!".format(key, myfile))
                    
                    field_list[key] = value_read
                    
    return field_list
    
    
def ExtractOutputDirectoryFromLuaFile(lua_file, log):
    """Read the Lua file and find the output directory in it.
    
    If output directory is not found or not usable (because there is no ${MOREFEM_RESULT_DIR} inside) 
    
    \param[in] log The opened file into which log assessments may be written.
    """
    
    ret = None
    
    with open(lua_file) as f:
        
        is_line_found = False
        
        for line in f:
            line = line.lstrip()
            
            if line.startswith('output_directory'):
                assert(is_line_found is False), "Only one line should be relevant in any given Lua file!"
                is_line_found = True
                
                pos = line.find('=')
                assert(pos > 0)
                line = line[pos + 1:]
                
                pos = line.find('#')
                if (pos > 0):
                    line = line[:pos]
                
                ret = line.strip(' ,"\n')
    
    
    # A demo needs to be usable in the test pipeline to use in its output directory the environment variable
    # ${MOREFEM_RESULT_DIR}.
    str_result_dir = "${MOREFEM_RESULT_DIR}"
    
    if str_result_dir not in ret:
        log.write("\t[ERROR] In Lua file {lua}, output_directory does not contain ${{MOREFEM_RESULT_DIR}} and "
        "can't therefore be used for test purposes.\n".format(lua=lua_file))                        
        return None
        
    if not ret:
        log.write("No output_directory found in Lua file {lua}.\n".format(lua=lua_file))                
    
    return ret
        



class CheckMoReFEM:
    

    """
    \param[in] morefem_path Absolute path that leads to the MoReFEM folder.
    \param[in] build_configuration_file Build configuration file to use (the same as the one provided to scons command).
    \param[in] target_list List of the executables to compile and the demo lua files to use agains them.
    \param[in] do_create_reference If True, the goal is to build the reference case against which runs will be compared.
    If False, each case is built and compared to the reference; if the former doesn't exist an error arises.
    \param[in] scons_path Path to the scons executable.    
    \param[in] stdout_file File in which all standard outputs of programs will be redirected.
    \param[in] stderr_file File in which all error outputs of programs will be redirected.
    """
    def  __init__(self, \
                  morefem_path, \
                  build_configuration_file, \
                  target_list, \
                  log_file,
                  stdout_file,
                  stderr_file,
                  do_create_reference = False,
                  scons_path = "/Users/Shared/Software/Scons/bin/scons"):
                      
        self.__morefem_path = morefem_path
        self.__scons_path = scons_path
        self.__build_configuration_file = build_configuration_file
        self.__do_create_reference = do_create_reference
        self.__stdout_file = stdout_file
        self.__stderr_file = stderr_file
        self.__work_dir = os.path.join("/Volumes", \
                                       "Data",
                                       os.environ["USER"],    
                                       "MoReFEM",
                                       "TestWorkDir")
        self.__log_file = log_file
                                       
        if not os.path.exists(self.__work_dir):
            os.makedirs(self.__work_dir)                                           
                                       
        
        self.__startswith_ignore_list = ("time_iteration.hhdata", "time_log.")
        
        assert(os.path.exists(morefem_path))
        
        self.InitLogFile()
        
       
        
        for target in target_list:
                
            model_name = target.executable
            
            target_list_full_path = (os.path.join(self.__morefem_path, item) for item in target.lua_file_list)
            
            self.TestModel(model_name, target_list_full_path, target.do_call_ensight_output, target.Nproc, target.is_sequential)
        
     
    def __del__(self):
        self.__log.close()
        
    def __RefreshLogFiles(self):        
        self.__log.close()
        self.__log = open(self.__log_file, 'a')
        
        self.__stdout.close()
        self.__stdout = open(self.__stdout_file, 'a')
        
        self.__stderr.close()
        self.__stderr = open(self.__stderr_file, 'a')
        
        
    def InitLogFile(self):
        
        field_list = ReadConfigurationFile(self.__build_configuration_file)
        self.__field_list = field_list           
        
       
                                
        self.__build_directory = os.path.join(field_list['BUILD_DIR'], \
                                              field_list['COMPILER_DIRECTORY'], \
                                              field_list['MODE'], \
                                              field_list['LIBRARY_TYPE'])
                                              
        self.__mpidir = os.path.normpath(os.path.join(field_list['OPEN_MPI_INCL_DIR'], '..'))
        
                                
        if os.path.exists(self.__log_file):
            os.remove(self.__log_file)                
                                
        print("Log file is {0}".format(self.__log_file))
        print("Standard output of SCons and programs is to be written in {0}".format(self.__stdout_file))
        print("Error output of SCons and programs is to be written in {0}".format(self.__stderr_file))
        
        self.__log = open(self.__log_file, 'w')
        
        self.__log.write("{0}\n\n".format(datetime.datetime.utcnow()))
        
        self.__stdout = open(self.__stdout_file, 'a')
        self.__stderr = open(self.__stderr_file, 'a')
        
     
    def TestModel(self, model_name, lua_file_list, do_call_ensight_output, Nproc, is_sequential):
        """
        
        \param[in] Nproc Number of processors to deal with in parallel. Put 0 or 1 if parallel is not relevant for the model.
        """
        
        self.__log.write("=============================================\n")
        self.__log.write("Compiling and running model {model}\n".format(model = model_name)) 
        self.__log.write("=============================================\n")

        if self.__Compile(model_name, do_call_ensight_output):
            for lua_file in lua_file_list:
                if ExtractOutputDirectoryFromLuaFile(lua_file, self.__log):
                    result_dir = self.__SetMoReFEMResultDir(model_name, lua_file, 1)
                    
                    # Sequential case: run and compare to existing reference.
                    if is_sequential and self.__SequentialRun(model_name, lua_file, do_call_ensight_output):
                 
                        if not self.__do_create_reference:
                            reference_dir = self.__ComputeResultDir(model_name, lua_file, is_reference_dir = True, Nproc = 1)
                        
                            if not os.path.exists(reference_dir):
                                self.__log.write("\tReference model output directory ({0}) doesn't exist!\n" .format(reference_dir))
                            else:                        
                                comp = CompareDirectories.CompareDirectories(result_dir, 
                                                                             reference_dir,
                                                                             self.__startswith_ignore_list,
                                                                             '\t')
                                                                     
                                if comp.AreIdentical():
                                    self.__log.write("\tOutput is the same as the reference model.\n")
                                else:
                                    self.__log.write("Discrepancy with reference model:\n")
                                    self.__log.write("{0}\n".format(comp))
                                    
                    # Parallel case: just run, as the current comparison by diffs wouldn't do for parallel case.
                    if Nproc > 1:
                        result_dir = self.__SetMoReFEMResultDir(model_name, lua_file, Nproc)
                        self.__ParallelRun(model_name, lua_file, do_call_ensight_output, Nproc)
                        
        
        self.__log.write("\n\n")
        
        # Steps to ensure the log may be consulted during the test process.
        self.__RefreshLogFiles()
        
        
    def __Compile(self, model_name, do_call_ensight_output):
        
        os.chdir(os.path.join(self.__morefem_path, "Sources"))
        
        cmd = (self.__scons_path,
                "-j",
                str(multiprocessing.cpu_count()),
                model_name,
                "--config_file={0}".format(self.__build_configuration_file))
                
        self.__PrintCmd(cmd)      
        
        scons_process = subprocess.Popen(cmd, shell=False, stdout=self.__stdout, stderr=self.__stderr)
        scons_process.communicate()
        
        if scons_process.returncode is not 0:
            self.__log.write("\tCompilation failed!\n")
            return False
        else:
            self.__log.write("\tSuccessful compilation.\n")
            
            if do_call_ensight_output:
                ret = self.__Compile("ensight4{}".format(model_name), do_call_ensight_output = False)
                
                if not ret:
                    return False
            
            return True
            
    
    def __SequentialRun(self, model_name, lua_file, do_call_ensight_output):
        
        cmd = (os.path.join(self.__build_directory, model_name),
               "--input_parameters",
               lua_file)
                                                      
        self.__PrintCmd(cmd)

        execution = subprocess.Popen(cmd, shell=False, stdout=self.__stdout, stderr=self.__stderr)  
        execution.communicate()
        
        if execution.returncode is not 0:
            self.__log.write("\tExecution with {lua} failed!\n".format(lua = lua_file))
            return False
        else:
            self.__log.write("\tSuccessful sequential run with {lua}.\n".format(lua = lua_file))
            
            if do_call_ensight_output:
                self.__EnsightOutput(model_name, lua_file)
            
            return True
            
            
    def __EnsightOutput(self, model_name, lua_file):
        
        cmd = (os.path.join(self.__build_directory, "ensight4{}".format(model_name)),
               "--input_parameters",
               lua_file)
                                                      
        self.__PrintCmd(cmd)

        execution = subprocess.Popen(cmd, shell=False, stdout=self.__stdout, stderr=self.__stderr)  
        execution.communicate()
        
        if execution.returncode is not 0:
            self.__log.write("\tEnsightOutput on {lua} failed!\n".format(lua = lua_file))
            return False
        else:
            self.__log.write("\tSuccessful EnsightOutput on {lua}.\n".format(lua = lua_file))
            return True            
            
            
    def __ParallelRun(self, model_name, lua_file, do_call_ensight_output, Nproc):
        
        cmd = (os.path.join(self.__mpidir, "bin", "mpirun"),
               "-np",
               str(Nproc),
               os.path.join(self.__build_directory, model_name),
               "--input_parameters",
               lua_file)
               
        lua_filename = os.path.split(lua_file)[1]                    
        assert(lua_filename.endswith(".lua"))
        lua_filename = lua_filename[:-4]               
               
        script_file = os.path.join(self.__work_dir,
                                   "mpi_{model}_{lua}.sh".format(model = model_name, lua = lua_filename))
        
        with open(script_file, 'w') as FILE:
            FILE.write("#! /bin/zsh\n")
            FILE.write(' '.join(cmd))
            
        subprocess.Popen(("chmod", "+x", script_file), shell=False).communicate()          
                     
        self.__PrintCmd(cmd)
        
        execution = subprocess.Popen(script_file, shell=False, stdout=self.__stdout, stderr=self.__stderr)  
        execution.communicate()

        if execution.returncode is not 0:
            self.__log.write("\tParallel execution with {lua} failed!\n".format(lua = lua_file))
            return False
        else:
            self.__log.write("\tSuccessful parallel run with {lua}.\n".format(lua = lua_file))
            
            if do_call_ensight_output:
                self.__EnsightOutput(model_name, lua_file)
            
            return True        
            
            
    def __PrintCmd(self, cmd):
        
        command_as_string = ' '.join(cmd)
        
        self.__stdout.write("\n\n===================================================\n")
        self.__stdout.write("COMMAND = {0}\n".format(command_as_string))
        self.__stdout.write("===================================================\n\n")
        self.__stdout.flush()
        
        
    def __SetMoReFEMResultDir(self, model_name, lua_file, Nproc = 1):
        """Set the environment variable MOREFEM_RESULT_DIR to its correct value for the model and Lua file under
        consideration.
        
        \param[in] lua_filename Path to the Lua file.
        \param[in] is_reference_dir True if what is requested is the result dir related to the reference output, or
        False if you want the result directory related to the current run.
        \param[in] Nproc Number of processors to consider. Use 1 for the sequential case.
        """
        morefem_result_dir =  self.__ComputeResultDir(model_name, lua_file, self.__do_create_reference, Nproc)
                                                
        os.environ["MOREFEM_RESULT_DIR"] = morefem_result_dir
        
        return morefem_result_dir
        
        
        
        
    def __ComputeResultDir(self, model_name, lua_file, is_reference_dir, Nproc):
        """
        \param[in] lua_filename Path to the Lua file.
        \param[in] is_reference_dir True if what is requested is the result dir related to the reference output, or
        False if you want the result directory related to the current run.
        \param[in] Nproc Number of processors to consider. Use 1 for the sequential case.
        """
        lua_filename = os.path.split(lua_file)[1]                    
        assert(lua_filename.endswith(".lua"))
        lua_filename = lua_filename[:-4]
        
        if Nproc > 1:
            lua_filename += "_mpi"
        
        return os.path.join("/Volumes", \
                            "Data",
                            os.environ["USER"],    
                            "MoReFEM",
                            is_reference_dir and "ReferenceModels" or "Tests",
                            self.__field_list['COMPILER_DIRECTORY'], \
                            self.__field_list['MODE'], \
                            self.__field_list['LIBRARY_TYPE'],
                            model_name,
                            lua_filename)
        
        
   
    
if __name__ == "__main__":
                        
    target_list = targets.TestAndModelTargetList()
    
    morefem_path = os.path.join("/Users", os.environ['USER'], "Codes", "MoReFEM")
    
    cases = ('clang_debug', 'clang_release', 'gcc_debug', 'gcc_release', 'clang_debug_macro', 'llvm_clang_debug')       
    
    for case in cases:
    
        stdout = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "test_stdout_{case}.txt".format(case = case))
        stderr = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "test_stderr_{case}.txt".format(case = case))
        log_file = os.path.join("/Volumes", "Data", os.environ["USER"], "MoReFEM", "TestWorkDir", "log_file_{case}.txt".format(case = case))
    
        if os.path.exists(stdout):
            os.remove(stdout)
        
        if os.path.exists(stderr):
            os.remove(stderr)
    
        CheckMoReFEM(morefem_path,
                        os.path.join(morefem_path, "Sources", "build_configuration_{case}.py".format(case = case)),
                        target_list,
                        log_file,
                        stdout,
                        stderr)
