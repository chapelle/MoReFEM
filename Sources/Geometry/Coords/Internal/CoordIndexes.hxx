///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 14:28:06 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            inline unsigned int CoordIndexes::GetFileIndex() const noexcept
            {
                assert(file_index_ != NumericNS::UninitializedIndex<unsigned int>());
                return file_index_;
            }


            inline unsigned int CoordIndexes::GetPositionInOriginalCoordList() const noexcept
            {
                assert(position_in_original_coord_list_ != NumericNS::UninitializedIndex<unsigned int>());
                return position_in_original_coord_list_;
            }


            inline unsigned int CoordIndexes::GetPositionInReducedCoordList() const noexcept
            {
                assert(position_in_reduced_coord_list_ != NumericNS::UninitializedIndex<unsigned int>());
                return position_in_reduced_coord_list_;
            }


        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_COORD_INDEXES_HXX_
