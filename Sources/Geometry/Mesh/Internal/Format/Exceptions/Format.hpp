///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_


# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Format
        {


            //! Generic exception.
            class Exception : public MoReFEM::Exception
            {
            public:


                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! Copy constructor.
                Exception(const Exception&) = default;

                //! Move constructor.
                Exception(Exception&&) = default;

                //! Copy affectation.
                Exception& operator=(const Exception&) = default;

                //! Move affectation.
                Exception& operator=(Exception&&) = default;



            };


            //! Thrown when mesh file couldn't be opened.
            class UnableToOpenFile final : public Exception
            {

            public:


                /*!
                 * \brief Constructor
                 *
                 * \param[in] mesh_file Mesh file being read
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnableToOpenFile(const std::string& mesh_file,
                                          const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~UnableToOpenFile();

                //! Copy constructor.
                UnableToOpenFile(const UnableToOpenFile&) = default;

                //! Move constructor.
                UnableToOpenFile(UnableToOpenFile&&) = default;

                //! Copy affectation.
                UnableToOpenFile& operator=(const UnableToOpenFile&) = default;

                //! Move affectation.
                UnableToOpenFile& operator=(UnableToOpenFile&&) = default;



            };



            //! Called when there is an attempt to write in Medit format a geometric elementtype not supported
            class UnsupportedGeometricElt final : public Exception
            {

            public:


                /*!
                 * \brief Constructor
                 *
                 * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered (eg 'Triangle3')
                 * \param[in] format Name of the format which doesn't support the element (e.g. 'Ensight')
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnsupportedGeometricElt(const std::string& geometric_elt_identifier,
                                                 const std::string& format,
                                                 const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~UnsupportedGeometricElt();

                //! Copy constructor.
                UnsupportedGeometricElt(const UnsupportedGeometricElt&) = default;

                //! Move constructor.
                UnsupportedGeometricElt(UnsupportedGeometricElt&&) = default;

                //! Copy affectation.
                UnsupportedGeometricElt& operator=(const UnsupportedGeometricElt&) = default;

                //! Move affectation.
                UnsupportedGeometricElt& operator=(UnsupportedGeometricElt&&) = default;

            };



        } // namespace Format


    } // namespace Exception


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_EXCEPTIONS_x_FORMAT_HPP_
