///
////// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorInstancesGroup
/// \addtogroup OperatorInstancesGroup
/// \{

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        FollowingPressure<TimeDependencyT>
        ::FollowingPressure(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                            const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                            elementary_data_type&& a_elementary_data,
                            const parameter_type& pressure)
        : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent(),
        pressure_(pressure)
        {
            const auto& elementary_data = GetElementaryData();

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const int Nnode_for_unkown = static_cast<int>(unknown_ref_felt.Nnode());

            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unkown = static_cast<int>(test_unknown_ref_felt.Nnode());

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            assert(mesh_dimension == 3 && "Operator restricted to 2D elements in a 3D mesh.");

            assert(unknown_ref_felt.GetFEltSpaceDimension() == 2 && "Operator restricted to 2D elements in a 3D mesh.");

            former_local_displacement_.resize(elementary_data.NdofRow());

            this->matrix_parent::InitLocalMatrixStorage({
                {
                    { Nnode_for_unkown, Nnode_for_test_unkown }, // phi_dphi_testdr
                    { Nnode_for_unkown, Nnode_for_test_unkown } // phi_dphi_testds
                }
            });

            this->vector_parent::InitLocalVectorStorage({
                {
                    mesh_dimension, // dxdr
                    mesh_dimension, // dxds
                    mesh_dimension // dxdr_cross_dxds
                }
            });
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        FollowingPressure<TimeDependencyT>::~FollowingPressure() = default;


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        const std::string& FollowingPressure<TimeDependencyT>::ClassName()
        {
            static std::string name("FollowingPressure");
            return name;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        void FollowingPressure<TimeDependencyT>::ComputeEltArray()
        {
            const auto& local_displacement = GetFormerLocalDisplacement();

            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            auto& vector_result = elementary_data.GetNonCstVectorResult();
            matrix_result.Zero(); // Is it necessary ?
            vector_result.Zero(); // Is it necessary ?

            auto& phi_dphi_testdr = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_dphi_testdr)>();
            auto& phi_dphi_testds = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_dphi_testds)>();

            auto& dxdr_at_quad_point = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr)>();
            auto& dxds_at_quad_point = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxds)>();
            auto& dxdr_cross_dxds_at_quad_point = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr_cross_dxds)>();

            const auto& infos_at_quad_pt_list_for_unknown = elementary_data.GetInformationsAtQuadraturePointListForUnknown();
            const auto& infos_at_quad_pt_list_for_test_unknown = elementary_data.GetInformationsAtQuadraturePointListForTestUnknown();

            assert(infos_at_quad_pt_list_for_unknown.size() == infos_at_quad_pt_list_for_test_unknown.size());

            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

            // Number of component of the unknown.
            const unsigned int Ncomponent = elementary_data.GetMeshDimension();

            const int int_Ncomponent = static_cast<int>(Ncomponent);

            const unsigned int Nnode_for_unknown = ref_felt.Nnode();
            const unsigned int Nnode_for_test_unknown = test_ref_felt.Nnode();

            const int int_Nnode_for_unknown = static_cast<int>(Nnode_for_unknown);
            const int int_Nnode_for_test_unknown = static_cast<int>(Nnode_for_test_unknown);

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            assert(geom_elt.GetDimension() == 2);

            const auto& pressure = GetPressure();

            auto& coords_in_geom_elt = GetNonCstWorkCoords();

            unsigned int quad_pt_index = 0;

            for (const auto& infos_at_quad_pt_for_unknown : infos_at_quad_pt_list_for_unknown)
            {
                const auto& infos_at_quad_pt_for_test_unknown = infos_at_quad_pt_list_for_test_unknown[quad_pt_index];

                const auto& phi = infos_at_quad_pt_for_unknown.GetRefFEltPhi();
                const auto& dphi = infos_at_quad_pt_for_unknown.GetGradientRefFEltPhi();

                const auto& dphi_test = infos_at_quad_pt_for_test_unknown.GetGradientRefFEltPhi();

                const auto& quad_pt = infos_at_quad_pt_for_unknown.GetQuadraturePoint();

                const double factor = quad_pt.GetWeight() * pressure.GetValue(quad_pt, geom_elt); // Here no jacobian.

                assert(infos_at_quad_pt_for_unknown.GetQuadraturePoint().GetRuleName() ==
                       infos_at_quad_pt_for_test_unknown.GetQuadraturePoint().GetRuleName());

                dxdr_at_quad_point.Zero();
                dxds_at_quad_point.Zero();

                // Here the two indices hardcoded 0 and 1 correspond to the two variables of derivation in a 2D element
                // but the vectors are in 3D, hence Ncomponent = 3.
                for (unsigned int node_index = 0; node_index < Nnode_for_unknown; ++node_index)
                {
                    unsigned int dof_index = node_index;

                    int int_node_index = static_cast<int>(node_index);

                    Advanced::GeomEltNS::Local2Global(geom_elt,
                                                      ref_felt.GetBasicRefFElt().GetLocalNode(node_index).GetLocalCoords(),
                                                      coords_in_geom_elt);

                    for (unsigned int component = 0; component < Ncomponent; ++component, dof_index += Nnode_for_unknown)
                    {
                        int int_component = static_cast<int>(component);

                        dxdr_at_quad_point(int_component) += dphi(int_node_index, 0) * (coords_in_geom_elt[component] + local_displacement[static_cast<std::size_t>(dof_index)]);
                        dxds_at_quad_point(int_component) += dphi(int_node_index, 1) * (coords_in_geom_elt[component] + local_displacement[static_cast<std::size_t>(dof_index)]);
                    }
                }

                // Same comment as previously.
                for (int i = 0; i < int_Nnode_for_unknown; ++i)
                {
                    for (int j = 0; j < int_Nnode_for_test_unknown; ++j)
                    {
                        phi_dphi_testdr(i, j) = phi(i) * dphi_test(j, 0);
                        phi_dphi_testds(i, j) = phi(i) * dphi_test(j, 1);
                    }
                }

                // The matrix is filled by block. Its size is (Nnode*Ncomponent)x(Nnode*Ncomponent).
                // Each block depends on a coordinate of dxdr and dxds.
                for (int i = 0; i < int_Nnode_for_unknown; ++i)
                {
                    for (int j = 0; j < int_Nnode_for_test_unknown; ++j)
                    {
                        // Block 12
                        matrix_result(i, j + int_Nnode_for_test_unknown) += factor * (- dxdr_at_quad_point(2) * phi_dphi_testds(i, j) - (- dxds_at_quad_point(2)) * phi_dphi_testdr(i, j));
                        // Block 13
                        matrix_result(i, j + 2*int_Nnode_for_test_unknown) += factor * (dxdr_at_quad_point(1) * phi_dphi_testds(i, j) - dxds_at_quad_point(1) * phi_dphi_testdr(i, j));
                        // Block 21
                        matrix_result(i + int_Nnode_for_unknown, j) += factor * (dxdr_at_quad_point(2) * phi_dphi_testds(i, j) - dxds_at_quad_point(2) * phi_dphi_testdr(i, j));
                        // Block 23
                        matrix_result(i + int_Nnode_for_unknown, j + 2*int_Nnode_for_test_unknown) += factor * (- dxdr_at_quad_point(0) * phi_dphi_testds(i, j) - (- dxds_at_quad_point(0)) * phi_dphi_testdr(i, j));
                        // Block 31
                        matrix_result(i + 2*int_Nnode_for_unknown, j) += factor * (- dxdr_at_quad_point(1) * phi_dphi_testds(i, j) - (- dxds_at_quad_point(1)) * phi_dphi_testdr(i, j));
                        // Block 32
                        matrix_result(i + 2*int_Nnode_for_unknown, j + int_Nnode_for_test_unknown) += factor * (dxdr_at_quad_point(0) * phi_dphi_testds(i, j) - dxds_at_quad_point(0) * phi_dphi_testdr(i, j));
                    }
                }

                // Cross product computation.
                dxdr_cross_dxds_at_quad_point(0) = dxdr_at_quad_point(1) * dxds_at_quad_point(2) - dxdr_at_quad_point(2) * dxds_at_quad_point(1);
                dxdr_cross_dxds_at_quad_point(1) = dxdr_at_quad_point(2) * dxds_at_quad_point(0) - dxdr_at_quad_point(0) * dxds_at_quad_point(2);
                dxdr_cross_dxds_at_quad_point(2) = dxdr_at_quad_point(0) * dxds_at_quad_point(1) - dxdr_at_quad_point(1) * dxds_at_quad_point(0);

                // Integration of the residual.
                for (int node_index = 0; node_index < int_Nnode_for_unknown; ++node_index)
                {
                    int dof_index = node_index;

                    const auto value = factor * phi(node_index);

                    for (int component = 0; component < int_Ncomponent; ++component, dof_index += Nnode_for_unknown)
                        vector_result(dof_index) += value * dxdr_cross_dxds_at_quad_point(component);
                }

                ++quad_pt_index;
            }
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const typename FollowingPressure<TimeDependencyT>::parameter_type&
        FollowingPressure<TimeDependencyT>::GetPressure() const noexcept
        {
            return pressure_;
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline const std::vector<double>&
        FollowingPressure<TimeDependencyT>::GetFormerLocalDisplacement() const noexcept
        {
            return former_local_displacement_;
        }



        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline std::vector<double>&
        FollowingPressure<TimeDependencyT>::GetNonCstFormerLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
        }


        template
        <
            template<ParameterNS::Type> class TimeDependencyT
        >
        inline SpatialPoint& FollowingPressure<TimeDependencyT>::GetNonCstWorkCoords() noexcept
        {
            return work_coords_;
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_FOLLOWING_PRESSURE_HXX_
