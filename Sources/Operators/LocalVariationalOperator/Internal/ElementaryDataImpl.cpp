///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 10:56:52 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup OperatorsGroup
/// \addtogroup OperatorsGroup
/// \{

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace LocalVariationalOperatorNS
        {
        
        
            ElementaryDataImpl::ElementaryDataImpl(const Internal::RefFEltNS::RefLocalFEltSpace& a_ref_felt_space,
                                                   const QuadratureRule& quadrature_rule,
                                                   const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                                   unsigned int felt_space_dimension,
                                                   unsigned int geometric_mesh_region_dimension,
                                                   AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            : ref_felt_space_(a_ref_felt_space),
            quadrature_rule_(quadrature_rule),
            Nnode_row_(0u),
            Nnode_col_(0u),
            Ndof_row_(0u),
            Ndof_col_(0u),
            felt_space_dimension_(felt_space_dimension),
            geometric_mesh_region_dimension_(geometric_mesh_region_dimension)
            {
                assert(felt_space_dimension <= geometric_mesh_region_dimension);
                
                const auto& ref_felt_space = GetRefLocalFEltSpace();
                
                const unsigned int Nquadrature_point = quadrature_rule.NquadraturePoint();
                const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();
                
                InitReferenceFiniteElements(unknown_storage);
                
                InitTestReferenceFiniteElements(test_unknown_storage);
                
                infos_at_quad_pt_list_for_unknown_.reserve(Nquadrature_point);
                infos_at_quad_pt_list_for_test_unknown_.reserve(Nquadrature_point);
                
                SetGeomEltDimension();
                
                Ncoords_in_geom_ref_elt_ = ref_geom_elt.Ncoords();
                
                point_.Resize(static_cast<int>(Ncoords_in_geom_ref_elt_), static_cast<int>(GetGeomEltDimension()));
                
                // Initialize correctly the informations at each quadrature point.
                for (unsigned int quadrature_point_index = 0; quadrature_point_index < Nquadrature_point; ++quadrature_point_index)
                {
                    const auto& quadrature_point = quadrature_rule.Point(quadrature_point_index);
                    
                    Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint item(quadrature_point,
                                                                                             ref_geom_elt,
                                                                                             GetRefFEltList(),
                                                                                             geometric_mesh_region_dimension_,
                                                                                             do_allocate_gradient_felt_phi);
                    
                    infos_at_quad_pt_list_for_unknown_.emplace_back(std::move(item));
                }
                
                assert(infos_at_quad_pt_list_for_unknown_.size() == Nquadrature_point);
                
                // Initialize correctly the informations at each quadrature point.
                for (unsigned int quadrature_point_index = 0; quadrature_point_index < Nquadrature_point; ++quadrature_point_index)
                {
                    const auto& quadrature_point = quadrature_rule.Point(quadrature_point_index);
                    
                    Advanced::LocalVariationalOperatorNS::InformationsAtQuadraturePoint item(quadrature_point,
                                                                                             ref_geom_elt,
                                                                                             GetTestRefFEltList(),
                                                                                             geometric_mesh_region_dimension_,
                                                                                             do_allocate_gradient_felt_phi);
                    
                    infos_at_quad_pt_list_for_test_unknown_.emplace_back(std::move(item));
                }
                
                assert(infos_at_quad_pt_list_for_test_unknown_.size() == Nquadrature_point);
            }

            
            void ElementaryDataImpl::UpdateCoordinates(const GeometricElt& geometric_element)
            {
                const unsigned int Ncomponent = this->GetGeomEltDimension();
                const unsigned int Nnode_in_geometric_element = this->NnodeInRefGeomElt();
                
                const auto& coords_list = geometric_element.GetCoordsList();
                assert(coords_list.size() == Nnode_in_geometric_element);
                
                for (unsigned int i = 0; i < Nnode_in_geometric_element; ++i)
                {
                    const auto& current_point_ptr = coords_list[i];
                    assert(!(!current_point_ptr));
                    
                    const auto& current_point = *current_point_ptr;
                    
                    for(unsigned int icoor = 0; icoor < Ncomponent; ++icoor)
                        point_(static_cast<int>(i), static_cast<int>(icoor)) = current_point[icoor];
                }
            }
            
            
            void ElementaryDataImpl::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
            {
                auto& infos_at_quad_pt_list_for_unknown = GetNonCstInformationsAtQuadraturePointListForUnknown();
                
                auto& infos_at_quad_pt_list_for_test_unknown = GetNonCstInformationsAtQuadraturePointListForTestUnknown();
                
                SetCurrentLocalFEltSpace(&local_felt_space);
                
                for (auto& infos_at_quad_pt : infos_at_quad_pt_list_for_unknown)
                    infos_at_quad_pt.ComputeLocalFEltSpaceData(local_felt_space);
                
                for (auto& infos_at_quad_pt : infos_at_quad_pt_list_for_test_unknown)
                    infos_at_quad_pt.ComputeLocalFEltSpaceData(local_felt_space);
            }
            
            
            
            void ElementaryDataImpl::SetGeomEltDimension()
            {
                geom_elt_dimension_ = GetRefLocalFEltSpace().GetRefGeomElt().GetDimension();
            }
            
            
            
            void ElementaryDataImpl::InitReferenceFiniteElements(const ExtendedUnknown::vector_const_shared_ptr& unknown_storage)
            {
                const auto& ref_felt_space = GetRefLocalFEltSpace();

                assert(ref_felt_list_.empty());
                
                assert(Nnode_row_ == 0u);
                assert(Ndof_row_ == 0u);
                
                for (const auto& extended_unknown_ptr : unknown_storage)
                {
                    const auto& ref_felt = ref_felt_space.GetRefFElt(*extended_unknown_ptr);
                    
                    // No std::make_unique here as friendship is not seen through it.
                    auto ref_felt_in_local_numbering_ptr =
                        new Advanced::RefFEltInLocalOperator(ref_felt,
                                                             Nnode_row_,
                                                             Ndof_row_);
                    
                    Nnode_row_ += ref_felt_in_local_numbering_ptr->Nnode();
                    Ndof_row_ += ref_felt_in_local_numbering_ptr->Ndof();
                    
                    ref_felt_list_.emplace_back(std::move(ref_felt_in_local_numbering_ptr));
                }
            }
            
            
            void ElementaryDataImpl::InitTestReferenceFiniteElements(const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage)
            {
                const auto& ref_felt_space = GetRefLocalFEltSpace();
                
                assert(test_ref_felt_list_.empty());
                
                assert(Nnode_col_ == 0u);
                assert(Ndof_col_ == 0u);
                
                for (const auto& extended_test_unknown_ptr : test_unknown_storage)
                {
                    const auto& ref_felt = ref_felt_space.GetRefFElt(*extended_test_unknown_ptr);
                    
                    // No std::make_unique here as friendship is not seen through it.
                    auto ref_felt_in_local_numbering_ptr =
                    new Advanced::RefFEltInLocalOperator(ref_felt,
                                                         Nnode_col_,
                                                         Ndof_col_);
                    
                    Nnode_col_ += ref_felt_in_local_numbering_ptr->Nnode();
                    Ndof_col_ += ref_felt_in_local_numbering_ptr->Ndof();
                    
                    test_ref_felt_list_.emplace_back(std::move(ref_felt_in_local_numbering_ptr));
                }
            }
            
            namespace // anonymous
            {
                
                
                const Advanced::RefFEltInLocalOperator& GetRefFEltImpl(const ExtendedUnknown& unknown,
                                                                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list);
                
                
            } // anonymous
            
            
            const Advanced::RefFEltInLocalOperator& ElementaryDataImpl::GetRefFElt(const ExtendedUnknown& unknown) const
            {
                const auto& ref_felt_list = GetRefFEltList();
                return GetRefFEltImpl(unknown, ref_felt_list);
            }
            
            
            const Advanced::RefFEltInLocalOperator& ElementaryDataImpl::GetTestRefFElt(const ExtendedUnknown& unknown) const
            {
                const auto& test_ref_felt_list = GetTestRefFEltList();
                return GetRefFEltImpl(unknown, test_ref_felt_list);
            }
            
            
            namespace // anonymous
            {
                
                
                const Advanced::RefFEltInLocalOperator& GetRefFEltImpl(const ExtendedUnknown& unknown,
                                                                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
                {
                    auto it = std::find_if(ref_felt_list.cbegin(),
                                           ref_felt_list.cend(),
                                           [&unknown](const auto& current_ref_felt)
                                           {
                                               assert(!(!current_ref_felt));
                                               return current_ref_felt->GetExtendedUnknown() == unknown;
                                           });
                    assert(it != ref_felt_list.cend());
                    assert(!(!(*it)));
                    return *(*it);
                }
                
                
            } // anonymous
        
        
        } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
