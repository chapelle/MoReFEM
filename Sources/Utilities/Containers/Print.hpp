///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Jun 2013 10:41:42 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup UtilitiesGroup
/// \addtogroup UtilitiesGroup
/// \{

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_



# include <cassert>
# include <iosfwd>
# include <tuple>
# include <iostream> // mandatory due to std::cout as default parameter

# include "Utilities/String/Traits.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /*!
         * \brief Print the content of a non associative container (list, vector, deque, array, etc...)
         *
         * \tparam StreamT Type of output stream considered
         * \tparam ContainerT Type of the container to be displayed
         *
         * \param[in,out] stream Output stream in which container will be displayed
         * \param[in] container Container displayed
         * \param[in] separator Separator between two entries of the contained
         * \param[in] opener Prefix used while displaying the container
         * \param[in] closer Suffix used while displaying the container
         *
         * In most cases templates parameters can be determined implicitly at compile time:
         * \code
         * std::vector<double> foo { 1., 2., 3., 10., 42. };
         * std::ostringstream oconv;
         * PrintContainer(foo, oconv, " ", "---", "---")
         * \endcode
         * This code yields:
         *   ---1. 2. 3. 10. 42.---
         */
        template
        <
            class ContainerT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintContainer(const ContainerT& container, StreamT& stream = std::cout,
                            StringT1 separator = ", ", StringT2 opener = "[", StringT3 closer = "]\n")
        {
            stream << opener;

            auto it = container.cbegin();
            auto end = container.cend();
            auto size = container.size();
            (void) end; // avoid compilation warning in release mode

            for (decltype(size) i = 0u; i + 1u < size; ++it, ++i)
            {
                assert(it != end);
                stream << *it << separator;
            }

            if (size > 0u)
                stream << *it;

            stream << closer;
        }


        /*!
         * \brief Print the \a N first elements of a non associative container (list, vector, deque, array, etc...)
         *
         * If container is smaller than \a N, print it completely.
         *
         * \tparam N Number of elements to display.
         * \tparam StreamT Type of output stream considered
         * \tparam ContainerT Type of the container to be displayed
         *
         * \param[in,out] stream Output stream in which container will be displayed
         * \param[in] container Container displayed
         * \param[in] separator Separator between two entries of the contained
         * \param[in] opener Prefix used while displaying the container
         * \param[in] closer Suffix used while displaying the container
         *
         * In most cases templates parameters can be determined implicitly at compile time:
         * \code
         * std::vector<double> foo { 1., 2., 3., 10., 42. };
         * std::ostringstream oconv;
         * PrintContainer(foo, oconv, " ", "---", "---")
         * \endcode
         * This code yields:
         *   ---1. 2. 3. 10. 42.---
         */
        template
        <
            unsigned int N,
            class ContainerT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintNelt(const ContainerT& container, StreamT& stream = std::cout,
                           StringT1 separator = ", ", StringT2 opener = "[", StringT3 closer = "]\n")
        {
            stream << opener;

            auto it = container.cbegin();
            auto end = container.cend();
            auto size = container.size();
            (void) end; // avoid compilation warning in release mode

            const auto stop = std::min(size, static_cast<decltype(size)>(N));

            for (decltype(size) i = 0; i + 1 < stop; ++it, ++i)
            {
                assert(it != end);
                stream << *it << separator;
            }

            if (size > 0u)
                stream << *it;

            if (N < size)
                stream << separator << "...";

            stream << closer;
        }



        /*!
         * \brief Print the content of a non associative container (list, vector, deque, array, etc...) after
         * dereferencing pointers.
         *
         * \tparam StreamT Type of output stream considered.
         * \tparam ContainerT Type of the container to be displayed.
         *
         * \param[in,out] stream Output stream in which container will be displayed.
         * \param[in] container Container displayed.
         * \param[in] separator Separator between two entries of the contained.
         * \param[in] opener Prefix used while displaying the container.
         * \param[in] closer Suffix used while displaying the container.

         * // \todo #279 Fusion it with standard PrintContainer, with additional parameter to indicate whether pointers
         * // should be deferenced or not (need a a bit of metaprogramming)
         */
        template
        <
            class ContainerT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintPointerContainer(const ContainerT& container, StreamT& stream = std::cout,
                                   StringT1 separator = ", ", StringT2 opener = "[", StringT3 closer = "]\n")
        {
            stream << opener;

            auto it = container.cbegin();
            auto end = container.cend();
            auto size = container.size();
            static_cast<void>(end); // avoid compilation warning in release mode

            for (decltype(size) i = 0u; i + 1u < size; ++it, ++i)
            {
                assert(it != end);
                stream << *(*it) << separator;
            }

            if (size > 0u)
                stream << *(*it);

            stream << closer;
        }




        /*!
         * \brief Print the keys of an associative container (set, map, multimap, unordered_map, ...)
         *
         * Essentially the same as PrintContainer(), except that only the keys are printed
         *
         * \tparam StreamT Type of output stream considered
         * \tparam AssociativeContainerT Type of the container to be displayed
         *
         * \param[in,out] stream Output stream in which container keys will be displayed
         * \param[in] container Container which keys are displayed
         * \param[in] separator Separator between two entries of the contained
         * \param[in] opener Prefix ued while displaying the container
         * \param[in] closer Suffix ued while displaying the container
         *
         * In most cases templates parameters can be determined implicitly at compile time:
         * \code
         * std::unordered_map<double, int> foo { {1., 1},
         *                                        {2., 2},
         *                                        {3., 10},
         *                                        {10., 52},
         *                                        {42., 2},
         * std::ostringstream oconv;
         * PrintKeys(foo, oconv, " ", "---", "---")
         * \endcode
         * This code yields:
         *   ---1. 2. 3. 10. 42.---
         */

        template
        <
            class AssociativeContainerT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintKeys(const AssociativeContainerT& container, StreamT& stream = std::cout,
                       StringT1 separator = ", ", StringT2 opener = "[", StringT3 closer = "]\n")
        {
            stream << opener;

            auto it = container.cbegin();
            auto end = container.cend();
            auto size = container.size();
            (void) end; // avoid compilation warning in release mode

            for (decltype(size) i = 0u; i + 1u < size; ++it, ++i)
            {
                assert(it != end);
                stream << it->first << separator;
            }

            if (size > 0u)
                stream << it->first;

            stream << closer;
        }



        /*!
         * \brief Print an associative container (set, map, multimap, unordered_map, ...)
         *
         * Essentially the same as PrintContainer()
         *
         * \tparam StreamT Type of output stream considered
         * \tparam AssociativeContainerT Type of the container to be displayed
         *
         * \param[in,out] stream Output stream in which container keys will be displayed
         * \param[in] container Container which content is  displayed.
         * \param[in] separator Separator between two entries of the container.
         * \param[in] opener Prefix ued while displaying the container.
         * \param[in] closer Suffix ued while displaying the container.
         *
         * In most cases templates parameters can be determined implicitly at compile time:
         * \code
         * std::unordered_map<double, int> foo { {1., 1},
         *                                        {2., 2},
         *                                        {3., 10},
         *                                        {10., 52},
         *                                        {42., 2},
         * std::ostringstream oconv;
         * PrintKeys(foo, oconv, " ", "---", "---")
         * \endcode
         * This code yields:
         *   ---(1., 1) (2., 2) (3., 10) (10., 52) (42., 2)---
         */

        template
        <
            class AssociativeContainerT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintAssociativeContainer(const AssociativeContainerT& container, StreamT& stream = std::cout,
                                       StringT1 separator = ", ", StringT2 opener = "[", StringT3 closer = "]\n")
        {
            stream << opener;

            auto it = container.cbegin();
            auto end = container.cend();
            auto size = container.size();
            (void) end; // avoid compilation warning in release mode

            for (decltype(size) i = 0u; i + 1u < size; ++it, ++i)
            {
                assert(it != end);
                stream << '(' << it->first << ", " << it->second << ')' << separator;
            }

            if (size > 0u)
                stream << '(' << it->first << ", " << it->second << ')';

            stream << closer;
        }



        namespace Impl
        {


            /*!
             ** \brief Facility to print elements of a tuple
             **
             ** Inspired by Nicolai M. Josuttis "The C++ standard library" page 74
             */
            template<class StreamT, unsigned int Index, unsigned int Max, class TupleT>
            struct PrintTupleHelper
            {


                /*!
                 * \brief Static function that does the actual work.
                 *
                 * \param[in,out] stream Stream to which the tuple will be printed.
                 * \param[in] t Tuple to display.
                 * \param[in] separator Separator between all elements of the tuple.
                 */
                template<typename StringT>
                static void Print(StreamT& stream, const TupleT& t, const StringT& separator)
                {
                    using EltTupleType = typename std::tuple_element<Index,TupleT>::type;

                    const auto quote =
                        Utilities::String::IsString<EltTupleType>::value ? "\"" : "";

                    stream << quote << std::get<Index>(t) << quote << (Index + 1 == Max ? "" : separator);
                    PrintTupleHelper<StreamT, Index + 1, Max, TupleT>::Print(stream, t, separator);
                }


            };

            //! Specialization used to stop recursive call.
            template<class StreamT, unsigned int Max, class TupleT>
            struct PrintTupleHelper<StreamT, Max, Max, TupleT>
            {

                //! Nothing done here in the specialization.
                template<typename StringT>
                static void Print(StreamT&, const TupleT& , const StringT&)
                { }

            };


        } // namespace Impl



        /*!
         * \brief Print the content of a tuple or a pair.
         *
         * Inspired by Nicolai M. Josuttis "The C++ standard library" page 74.
         *
         * \tparam StreamT Type of output stream considered
         *
         * \param[in,out] stream Output stream in which tuple content will be displayed. All tuple elements must
         * define operator<<.
         * \param[in] tuple Tuple which content is  displayed.
         * \param[in] separator Separator between two entries of the tuple.
         * \param[in] opener Prefix ued while displaying the tuple.
         * \param[in] closer Suffix ued while displaying the tuple.

         */
        template
        <
            class TupleT,
            typename StreamT = std::ostream,
            typename StringT1 = std::string,
            typename StringT2 = std::string,
            typename StringT3 = std::string
        >
        void PrintTuple(const TupleT& tuple,
                        StreamT& stream = std::cout,
                        StringT1 separator = ",",
                        StringT2 opener = "[",
                        StringT3 closer = "]\n")
        {
            stream << opener;
            enum { size = std::tuple_size<TupleT>::value };
            Impl::PrintTupleHelper<StreamT, 0, size, TupleT>::Print(stream, tuple, separator);
            stream << closer;
        }



    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_HPP_
