///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup GeometryGroup
/// \addtogroup GeometryGroup
/// \{

#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE6_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE6_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Triangle/Triangle6.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Triangle6 geometric element read in a mesh.
     *
     * We are not considering here a generic Triangle6 object (that's the role of MoReFEM::RefGeomEltNS::Triangle6), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Triangle6 : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Triangle6>
    {
    public:

        //! Minimal constructor: only the GeometricMeshRegion is provided.
        explicit Triangle6(unsigned int mesh_unique_id);

        //! Constructor from stream: 'Ncoords' are read in the stream and object is built from them.
        explicit Triangle6(unsigned int mesh_unique_id,
                           const Coords::vector_unique_ptr& mesh_coords_list,
                           std::istream& stream);

        //! Constructor from vector of coords.
        explicit Triangle6(unsigned int mesh_unique_id,
                           const Coords::vector_unique_ptr& mesh_coords_list,
                           std::vector<unsigned int>&& coords);

        //! Destructor.
        ~Triangle6() override;

        //! Copy constructor.
        Triangle6(const Triangle6&) = default;

        //! Move constructor.
        Triangle6(Triangle6&&) = default;

        //! Copy affectation.
        Triangle6& operator=(const Triangle6&) = default;

        //! Move affectation.
        Triangle6& operator=(Triangle6&&) = default;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Triangle6& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE6_HPP_
