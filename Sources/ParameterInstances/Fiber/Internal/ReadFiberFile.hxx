///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:00:37 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup ParameterInstancesGroup
/// \addtogroup ParameterInstancesGroup
/// \{

#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            namespace // anonymous
            {


                //! Count the number of fibers in the fiber file.
                template<class TraitsT>
                unsigned int NfiberInFile(const std::string& fiber_file);



            } // namespace anonymous



            template<ParameterNS::Type TypeT>
            void ReadFiberFile(const ::MoReFEM::Wrappers::Mpi& mpi,
                               const std::string& fiber_file,
                               const GeometricMeshRegion& geometric_mesh_region,
                               const Domain& domain, // \todo #1130 Take the domain into account.
                               typename Traits<TypeT>::value_list_per_coord_index_type& out)
            {
                out.max_load_factor(Utilities::DefaultMaxLoadFactor());

                std::ifstream stream;
                FilesystemNS::File::Read(stream, fiber_file, __FILE__, __LINE__);

                std::string line;

                // Read the line "Vector per node".
                getline(stream, line);

                Utilities::String::StripRight(line, " \t");

                using traits = Traits<TypeT>;

                traits::CheckFirstLineOfFile(fiber_file, line);

                // Now read the content of the file and filter out in the process all that are not managed by current processor.
                constexpr long value_size = 12;
                std::string str_value(value_size, ' ');

                unsigned int line_count = 0u;

                const auto& processor_wise_coords_list = geometric_mesh_region.GetProcessorWiseCoordsList();
                const auto coords_begin = processor_wise_coords_list.cbegin();
                const auto coords_end = processor_wise_coords_list.cend();

                assert(std::none_of(coords_begin, coords_end,
                                    Utilities::IsNullptr<Coords::unique_ptr>));

                auto it_current_coords = coords_begin;

                typename traits::value_list_type value_list_for_current_coord;

                constexpr auto Nvertex_per_line = traits::NvertexPerLine();
                constexpr auto Nvalue_per_vertex = traits::NvaluePerVertex();

                // I need the total number of coords to make sure fiber file is not too long.
                const auto Ncoords_in_domain_program_wise = NcoordsInDomain<MpiScale::program_wise>(mpi,
                                                                                                    domain,
                                                                                                    geometric_mesh_region);

                #ifndef NDEBUG
                const auto Ncoords_in_domain_processor_wise = NcoordsInDomain<MpiScale::processor_wise>(mpi,
                                                                                                        domain,
                                                                                                        geometric_mesh_region);
                #endif // NDEBUG

                // =============================================
                // First check the number of vertices in the mesh is coherent with number of fibers.
                // =============================================

                if (mpi.IsRootProcessor())
                {
                    const auto Nfiber_in_file = NfiberInFile<traits>(fiber_file);

                    if (Nfiber_in_file != Ncoords_in_domain_program_wise)
                        throw Exception("Discrepancy between domain and fiber file: "
                                        + std::to_string(Nfiber_in_file) + " were read in the fiber file while "
                                        "the domain was composed of " + std::to_string(Ncoords_in_domain_program_wise) + " vertices.",
                                        __FILE__, __LINE__);
                }


                // =============================================
                // Then read the file and extract relevant data for current processor.
                // =============================================

                unsigned int Nvertex_read_in_fiber_file = 0u;

                while (getline(stream, line))
                {
                    const auto begin_line = line.cbegin();
                    const auto end_line = line.cend();

                    for (unsigned int vertex_on_line = 0u;
                         vertex_on_line < Nvertex_per_line && it_current_coords < coords_end;
                         ++vertex_on_line)
                    {
                        const auto begin_current_coord_info = begin_line + vertex_on_line * Nvalue_per_vertex * value_size;

                        if (begin_current_coord_info >= end_line) // to account for the very last line that might cover only one Coord.
                                                                  // Consistency check already performed in NfiberInFile.
                            continue;

                        const bool is_current_coord_a_match =
                            ((*it_current_coords)->GetPositionInCoordsListInMesh<MpiScale::program_wise>() == Nvertex_read_in_fiber_file);

                        ++Nvertex_read_in_fiber_file;

                        if (is_current_coord_a_match) // means current Coord is handled by current processor.
                        {


                            for (unsigned int value_index = 0u; value_index < Nvalue_per_vertex; ++value_index)
                            {
                                const auto begin_value = begin_current_coord_info + value_index * value_size;
                                assert(begin_value < end_line);

                                str_value.assign(begin_value, begin_value + value_size);
                                value_list_for_current_coord[value_index] = std::stod(str_value);
                            }

                            auto check = out.insert({(*it_current_coords)->GetIndex(), value_list_for_current_coord});
                            assert(check.second);
                            static_cast<void>(check);

                            ++it_current_coords;

                        }


                    } // for (unsigned int vertex_on_line ...

                    ++line_count;
                }

                assert(out.size() == static_cast<std::size_t>(Ncoords_in_domain_processor_wise)
                       && "Incorrect number of fibers read on current processor.");

            }


            namespace // anonymous
            {


                template<class TraitsT>
                unsigned int NfiberInFile(const std::string& fiber_file)
                {
                    constexpr auto Nvertex_per_line = TraitsT::NvertexPerLine();
                    constexpr auto Nvalue_per_vertex = TraitsT::NvaluePerVertex();

                    std::ifstream stream;
                    FilesystemNS::File::Read(stream, fiber_file, __FILE__, __LINE__);

                    std::string line;

                    // Read the line "Vector per node".
                    getline(stream, line);
                    Utilities::String::StripRight(line, " \t");
                    TraitsT::CheckFirstLineOfFile(fiber_file, line);

                    constexpr long value_size = 12;

                    unsigned int ret = 0u;

                    while (getline(stream, line))
                    {
                        const auto begin_line = line.cbegin();
                        const auto end_line = line.cend();

                        for (unsigned int vertex_on_line = 0u; vertex_on_line < Nvertex_per_line; ++vertex_on_line)
                        {
                            const auto begin_current_coord_info = begin_line + vertex_on_line * Nvalue_per_vertex * value_size;

                            if (begin_current_coord_info >= end_line) // to account for the very last line that might cover only one Coord.
                                continue;

                            ++ret;
                        }
                    }

                    return ret;
                }



            } // namespace anonymous


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_READ_FIBER_FILE_HXX_
