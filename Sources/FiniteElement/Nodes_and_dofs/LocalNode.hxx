///
////// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 30 Sep 2014 10:12:25 +0200
/// Copyright (c) Inria. All rights reserved.
///
/// \ingroup FiniteElementGroup
/// \addtogroup FiniteElementGroup
/// \{

#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HXX_


namespace MoReFEM
{


    inline const RefGeomEltNS::TopologyNS::LocalInterface& LocalNode::GetLocalInterface() const noexcept
    {
        return local_interface_;
    }


    inline unsigned int LocalNode::GetIndex() const noexcept
    {
        assert(index_ != NumericNS::UninitializedIndex<unsigned int>());
        return index_;
    }


    inline const LocalCoords& LocalNode::GetLocalCoords() const noexcept
    {
        return local_coords_;
    }


    inline bool operator<(const LocalNode& lhs, const LocalNode& rhs)
    {
        return lhs.GetIndex() < rhs.GetIndex();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_LOCAL_NODE_HXX_
