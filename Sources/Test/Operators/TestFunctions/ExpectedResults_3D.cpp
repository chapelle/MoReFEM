/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 16 Sep 2017 22:22:57 +0200
/// Copyright (c) Inria. All rights reserved.
///

#include "Test/Operators/TestFunctions/ExpectedResults.hpp"


namespace MoReFEM
{
    
    
    namespace TestFunctionsNS
    {

        
        expected_results_type<IsMatrixOrVector::matrix> GetExpectedMatricialResults3D(bool is_parallel)
        {
            expected_results_type<IsMatrixOrVector::matrix> expected_results;
            
            constexpr double one_60th = 1. / 60.;
            constexpr double one_120th = 1. / 120.;
            
            using content_type = content_type<IsMatrixOrVector::matrix>;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1",
                                 content_type
                                 {
                                     { one_60th, one_120th, one_120th, one_120th },
                                     { one_120th, one_60th, one_120th, one_120th },
                                     { one_120th, one_120th, one_60th, one_120th },
                                     { one_120th, one_120th, one_120th, one_60th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2",
                                 content_type
                                 {
                                     { one_60th, one_120th, one_120th, one_120th },
                                     { one_120th, one_60th, one_120th, one_120th },
                                     { one_120th, one_120th, one_60th, one_120th },
                                     { one_120th, one_120th, one_120th, one_60th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_1",
                                 content_type
                                 {
                                     { one_60th, -0., one_120th, -0., one_120th, -0., one_120th, -0. },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, -0., one_60th, -0., one_120th, -0., one_120th, -0. },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, -0., one_120th, -0., one_60th, -0., one_120th, -0. },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, -0., one_120th, -0., one_120th, -0., one_60th, -0. },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            

            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_2",
                                 content_type
                                 {
                                     { one_60th, one_60th, one_120th, one_120th, one_120th, one_120th, one_120th, one_120th },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, one_120th, one_60th, one_60th, one_120th, one_120th, one_120th, one_120th },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, one_120th, one_120th, one_120th, one_60th, one_60th, one_120th, one_120th },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                     { one_120th, one_120th, one_120th, one_120th, one_120th, one_120th, one_60th, one_60th },
                                     { -0., -0., -0., -0., -0., -0., -0., -0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2_potential_1",
                                 content_type
                                 {
                                     { one_60th, one_60th, one_120th, one_120th, one_120th, one_120th, one_120th, one_120th },
                                     { one_60th, -0., one_120th, -0., one_120th, -0., one_120th, -0. },
                                     { one_120th, one_120th, one_60th, one_60th, one_120th, one_120th, one_120th, one_120th },
                                     { one_120th, -0., one_60th, -0., one_120th, -0., one_120th, -0. },
                                     { one_120th, one_120th, one_120th, one_120th, one_60th, one_60th, one_120th, one_120th },
                                     { one_120th, -0., one_120th, -0., one_60th, -0., one_120th, -0. },
                                     { one_120th, one_120th, one_120th, one_120th, one_120th, one_120th, one_60th, one_60th },
                                     { one_120th, -0., one_120th, -0., one_120th, -0., one_60th, -0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_2_potential_2",
                                 content_type
                                 {
                                     { one_60th, one_60th, one_120th, one_120th, one_120th, one_120th, one_120th, one_120th },
                                     { one_60th, one_60th, one_120th, one_120th, one_120th, one_120th, one_120th, one_120th },
                                     { one_120th, one_120th, one_60th, one_60th, one_120th, one_120th, one_120th, one_120th },
                                     { one_120th, one_120th, one_60th, one_60th, one_120th, one_120th, one_120th, one_120th },
                                     { one_120th, one_120th, one_120th, one_120th, one_60th, one_60th, one_120th, one_120th },
                                     { one_120th, one_120th, one_120th, one_120th, one_60th, one_60th, one_120th, one_120th },
                                     { one_120th, one_120th, one_120th, one_120th, one_120th, one_120th, one_60th, one_60th },
                                     { one_120th, one_120th, one_120th, one_120th, one_120th, one_120th, one_60th, one_60th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double one_420th = 1. / 420.;
            constexpr double one_2520th = 1. / 2520. ;
            constexpr double one_630th = 1. / 630.;
            constexpr double one_315th = 1. / 315.;

            if (!is_parallel)
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_3",
                                     content_type
                                     {
                                         { one_420th, one_2520th, one_2520th, one_2520th, -one_630th, -one_420th, -one_630th, -one_630th, -one_420th, -one_420th },
                                         { one_2520th, one_420th, one_2520th, one_2520th, -one_630th, -one_630th, -one_420th, -one_420th, -one_630th, -one_420th },
                                         { one_2520th, one_2520th, one_420th, one_2520th, -one_420th, -one_630th, -one_630th, -one_420th, -one_420th, -one_630th },
                                         { one_2520th, one_2520th, one_2520th, one_420th, -one_420th, -one_420th, -one_420th, -one_630th, -one_630th, -one_630th },
                                         { -one_630th, -one_630th, -one_420th, -one_420th, 4. * one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, one_315th },
                                         { -one_420th, -one_630th, -one_630th, -one_420th, 2. * one_315th, 4. * one_315th, 2. * one_315th, one_315th, 2. * one_315th, 2. * one_315th },
                                         { -one_630th, -one_420th, -one_630th, -one_420th, 2. * one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th, one_315th, 2. * one_315th },
                                         { -one_630th, -one_420th, -one_420th, -one_630th, 2. * one_315th, one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th, 2. * one_315th },
                                         { -one_420th, -one_630th, -one_420th, -one_630th, 2. * one_315th, 2. * one_315th, one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th },
                                         { -one_420th, -one_420th, -one_630th, -one_630th, one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, 4. * one_315th },

                                     },
                                     expected_results,
                                     __FILE__, __LINE__);
            }
            else
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_3",
                                                         content_type
                                                         {
                                                             { one_420th, one_2520th, one_2520th, one_2520th, -one_420th, -one_630th, -one_420th, -one_630th, -one_630th, -one_420th },
                                                             { one_2520th, one_420th, one_2520th, one_2520th, -one_630th, -one_630th, -one_630th, -one_420th, -one_420th, -one_420th },
                                                             { one_2520th, one_2520th, one_420th, one_2520th, -one_420th, -one_420th, -one_630th, -one_630th, -one_420th, -one_630th },
                                                             { one_2520th, one_2520th, one_2520th, one_420th, -one_630th, -one_420th, -one_420th, -one_420th, -one_630th, -one_630th },
                                                             { -one_420th, -one_630th, -one_420th, -one_630th, 4. * one_315th, 2. * one_315th, 2. * one_315th, one_315th, 2. * one_315th, 2. * one_315th },
                                                             { -one_630th, -one_630th, -one_420th, -one_420th, 2. * one_315th, 4. * one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, one_315th },
                                                             { -one_420th, -one_630th, -one_630th, -one_420th, 2. * one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th, one_315th, 2. * one_315th },
                                                             { -one_630th, -one_420th, -one_630th, -one_420th, one_315th, 2. * one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th, 2. * one_315th },
                                                             { -one_630th, -one_420th, -one_420th, -one_630th, 2. * one_315th, 2. * one_315th, one_315th, 2. * one_315th, 4. * one_315th, 2. * one_315th },
                                                             { -one_420th, -one_420th, -one_630th, -one_630th, 2. * one_315th, one_315th, 2. * one_315th, 2. * one_315th, 2. * one_315th, 4. * one_315th },
                                                         },
                                                         expected_results,
                                                         __FILE__, __LINE__);
                
                
            }
            
            
            constexpr double one_90th = 1. / 90.;
            constexpr double one_180th = 1. / 180.;
            constexpr double one_360th = 1. / 360.;
            
            if (!is_parallel)
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_3",
                                     content_type
                                     {
                                         { 0., -one_360th, -one_360th, -one_360th, one_90th, one_180th, one_90th, one_90th, one_180th, one_180th },
                                         { -one_360th, 0., -one_360th, -one_360th, one_90th, one_90th, one_180th, one_180th, one_90th, one_180th },
                                         { -one_360th, -one_360th, 0., -one_360th, one_180th, one_90th, one_90th, one_180th, one_180th, one_90th },
                                         { -one_360th, -one_360th, -one_360th, 0., one_180th, one_180th, one_180th, one_90th, one_90th, one_90th },
                                     },
                                     expected_results,
                                     __FILE__, __LINE__);
            }
            else
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("mass_operator_potential_1_potential_3",
                                     content_type
                                     {
                                         { 0., -one_360th, -one_360th, -one_360th, one_180th, one_90th, one_180th, one_90th, one_90th, one_180th },
                                         { -one_360th, 0., -one_360th, -one_360th, one_90th, one_90th, one_90th, one_180th, one_180th, one_180th },
                                         { -one_360th, -one_360th, 0., -one_360th, one_180th, one_180th, one_90th, one_90th, one_180th, one_90th },
                                         { -one_360th, -one_360th, -one_360th, 0., one_90th, one_180th, one_180th, one_180th, one_90th, one_90th },
                                     },
                                     expected_results,
                                     __FILE__, __LINE__);
                
            }
            
            constexpr double one_6th = 1. / 6.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1",
                                 content_type
                                 {
                                     { .5, -one_6th, -one_6th, -one_6th },
                                     { -one_6th, one_6th, 0., 0. },
                                     { -one_6th, 0., one_6th, 0. },
                                     { -one_6th, 0., 0., one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_1",
                                 content_type
                                 {
                                     { .5, 0., -one_6th, 0., -one_6th, 0., -one_6th, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, 0., one_6th, 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, 0., 0., 0., one_6th, 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, 0., 0., 0., 0., 0., one_6th, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_2",
                                 content_type
                                 {
                                     { .5, .5, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, one_6th, one_6th, 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, one_6th, 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., one_6th, one_6th },
                                     { 0., 0., 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_2_potential_1",
                                 content_type
                                 {
                                     { .5, .5, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th },
                                     { .5, 0., -one_6th, 0., -one_6th, 0., -one_6th, 0. },
                                     { -one_6th, -one_6th, one_6th, one_6th, 0., 0., 0., 0. },
                                     { -one_6th, 0., one_6th, 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, one_6th, 0., 0. },
                                     { -one_6th, 0., 0., 0., one_6th, 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., one_6th, one_6th },
                                     { -one_6th, 0., 0., 0., 0., 0., one_6th, 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_2_potential_2",
                                 content_type
                                 {
                                     { .5, .5, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th },
                                     { .5, .5, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th, -one_6th },
                                     { -one_6th, -one_6th, one_6th, one_6th, 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, one_6th, one_6th, 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, one_6th, 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, one_6th, 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., one_6th, one_6th },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., one_6th, one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double one_30th = 1. / 30.;
            constexpr double one_15th = 1. / 15.;
            
            if (!is_parallel)
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_3",
                                     content_type
                                     {
                                         { .3, one_30th, one_30th, one_30th, -.2, one_15th, -.2, -.2, one_15th, one_15th },
                                         { one_30th, .1, 0., 0., -2. * one_15th, -one_30th, one_30th, one_30th, -one_30th, 0. },
                                         { one_30th, 0., .1, 0., one_30th, -one_30th, -2. * one_15th, one_30th, 0., -one_30th },
                                         { one_30th, 0., 0., .1, one_30th, 0., one_30th, -2. * one_15th, -one_30th, -one_30th },
                                         { -.2, -2. * one_15th, one_30th, one_30th, .8, -4. * one_15th, 2. * one_15th, 2. * one_15th, -4. * one_15th, -4. * one_15th },
                                         { one_15th, -one_30th, -one_30th, 0., -4. * one_15th, 8. * one_15th, -4. * one_15th, -4. * one_15th, 2. * one_15th, 2. * one_15th },
                                         { -.2, one_30th, -2. * one_15th, one_30th, 2. * one_15th, -4. * one_15th, .8, 2. * one_15th, -4. * one_15th, -4. * one_15th },
                                         { -.2, one_30th, one_30th, -2. * one_15th, 2. * one_15th, -4. * one_15th, 2. * one_15th, .8, -4. * one_15th, -4. * one_15th },
                                         { one_15th, -one_30th, 0., -one_30th, -4. * one_15th, 2. * one_15th, -4. * one_15th, -4. * one_15th, 8. * one_15th, 2. * one_15th },
                                         { one_15th, 0., -one_30th, -one_30th, -4. * one_15th, 2. * one_15th, -4. * one_15th, -4. * one_15th, 2. * one_15th, 8. * one_15th },
                                     },
                                     expected_results,
                                     __FILE__, __LINE__);
            }
            else
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_3",
                                     content_type
                                     {
                                         { 3. * .1, one_30th, one_30th, one_30th, one_15th, -.2, one_15th, -.2, -.2, one_15th },
                                         { one_30th, .1, 0., 0., -one_30th, -2. * one_15th, -one_30th, one_30th, one_30th, 0. },
                                         { one_30th, 0., .1, 0., 0., one_30th, -one_30th, -2. * one_15th, one_30th, -one_30th },
                                         { one_30th, 0., 0., .1, -one_30th, one_30th, 0., one_30th, -2. * one_15th, -one_30th },
                                         { one_15th, -one_30th, 0., -one_30th, 8. * one_15th, -4. * one_15th, 2. * one_15th, -4. * one_15th, -4. * one_15th, 2. * one_15th },
                                         { -.2, -2. * one_15th, one_30th, one_30th, -4. * one_15th, 4. * .2, -4. * one_15th, 2. * one_15th, 2. * one_15th, -4. * one_15th },
                                         { one_15th, -one_30th, -one_30th, 0., 2. * one_15th, -4. * one_15th, 8. * one_15th, -4. * one_15th, -4. * one_15th, 2. * one_15th },
                                         { -.2, one_30th, -2. * one_15th, one_30th, -4. * one_15th, 2. * one_15th, -4. * one_15th, 4. * .2, 2. * one_15th, -4. * one_15th },
                                         { -.2, one_30th, one_30th, -2. * one_15th, -4. * one_15th, 2. * one_15th, -4. * one_15th, 2. * one_15th, 4. * .2, -4. * one_15th },
                                         { one_15th, 0., -one_30th, -one_30th, 2. * one_15th, -4. * one_15th, 2. * one_15th, -4. * one_15th, -4. * one_15th, 8. * one_15th },
                                     },
                                     expected_results,
                                     __FILE__, __LINE__);
            }
            
            
            
            constexpr double one_3rd = 1. / 3.;
            
            if (!is_parallel)
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_3",
                                 content_type
                                 {
                                     { 0., 0., 0., 0., one_3rd, -one_3rd, one_3rd, one_3rd, -one_3rd, -one_3rd },
                                     { 0., 0., 0., 0., 0., one_6th, -one_6th, -one_6th, one_6th, 0. },
                                     { 0., 0., 0., 0., -one_6th, one_6th, 0., -one_6th, 0., one_6th },
                                     { 0., 0., 0., 0., -one_6th, 0., -one_6th, 0., one_6th, one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            }
            else
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("stiffness_operator_potential_1_potential_3",
                                 content_type
                                 {
                                     { 0., 0., 0., 0., -one_3rd, one_3rd, -one_3rd, one_3rd, one_3rd, -one_3rd },
                                     { 0., 0., 0., 0., one_6th, 0., one_6th, -one_6th, -one_6th, 0. },
                                     { 0., 0., 0., 0., 0., -one_6th, one_6th, 0., -one_6th, one_6th },
                                     { 0., 0., 0., 0., one_6th, -one_6th, 0., -one_6th, 0., one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            }
            
            InsertNewEntry<IsMatrixOrVector::matrix>("variable_mass_operator_potential_1",
                                 content_type
                                 {
                                     { one_60th, one_120th, one_120th, one_120th },
                                     { one_120th, one_60th, one_120th, one_120th },
                                     { one_120th, one_120th, one_60th, one_120th },
                                     { one_120th, one_120th, one_120th, one_60th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double two_3rd = 2./ 3.;
            constexpr double one_12th = 1. / 12.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_operator",
                                 content_type
                                 {
                                     { 5. * one_6th, 5. * one_6th, -one_3rd, -one_3rd, -one_3rd, -one_3rd, -one_6th, -one_6th },
                                     { 5. * one_6th, 5. * one_3rd, -one_3rd, -two_3rd, -one_3rd, -two_3rd, -one_6th, -one_3rd },
                                     { -one_3rd, -one_3rd, .25, .25, one_12th, one_12th, 0., 0. },
                                     { -one_3rd, -two_3rd, .25, .5, one_12th, one_6th, 0., 0. },
                                     { -one_3rd, -one_3rd, one_12th, one_12th, .25, .25, 0., 0. },
                                     { -one_3rd, -two_3rd, one_12th, one_6th, .25, .5, 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., one_6th, one_6th },
                                     { -one_6th, -one_3rd, 0., 0., 0., 0., one_6th, one_3rd },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("bidomain_potential_124_operator",
                                 content_type
                                 {
                                     { 5. * one_6th, 5. * one_6th, 0., -one_3rd, -one_3rd, 0., -one_3rd, -one_3rd, 0., -one_6th, -one_6th, 0. },
                                     { 5. * one_6th, 5. * one_3rd, 0., -one_3rd, -two_3rd, 0., -one_3rd, -two_3rd, 0., -one_6th, -one_3rd, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_3rd, -one_3rd, 0., .25, .25, 0., one_12th, one_12th, 0., 0., 0., 0. },
                                     { -one_3rd, -two_3rd, 0., .25, .5, 0., one_12th, one_6th, 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_3rd, -one_3rd, 0., one_12th, one_12th, 0., .25, .25, 0., 0., 0., 0. },
                                     { -one_3rd, -two_3rd, 0., one_12th, one_6th, 0., .25, .5, 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., 0., 0., 0., 0., 0., one_6th, one_6th, 0. },
                                     { -one_6th, -one_3rd, 0., 0., 0., 0., 0., 0., 0., one_6th, one_3rd, 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("grad_phi_tau_tau_grad_phi_operator",
                                 content_type
                                 {
                                     { 5. * one_6th, -one_3rd, -one_3rd, -one_6th },
                                     { -one_3rd, .25, one_12th, 0. },
                                     { -one_3rd, one_12th, .25, 0. },
                                     { -one_6th, 0., 0., one_6th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("grad_phi_tau_ortho_tau_grad_phi_operator",
                                                     content_type
                                                     {
                                                         { 4.22732, -1.54549, -1.54549, -1.13634 },
                                                         { -1.54549, 2.08353, -0.179377, -0.358662 },
                                                         { -1.54549, -0.179377,  2.08353, -0.358662 },
                                                         { -1.13634, -0.358662, -0.358662, 1.85366 },
                                                     },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            if (!is_parallel)
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("surfacic_bidomain_operator",
                                                         content_type
                                                         {
                                                             { 4.22732, 1.40911, -1.54549, -0.515164, -1.54549, -0.515164, -1.13634, -0.378779, 1.90911, -2.3031, 1.90911, 1.81822, -1.66667, -1.66667 },
                                                             { 1.40911, 8.45465, -0.515164, 1.03033, -0.515164, 1.03033, -0.378779, 0.757559, -4.12132, -2.22045e-16, -4.12132, -3.03023, -3.33067e-16, -3.33067e-16 },
                                                             { -1.54549, -0.515164, 2.08353, 0.694511, -0.179377, -0.0597923, -0.358662, -0.119554, -0.15155, 1.62977, -1., -0.909108, 1.29983, -0.868935 },
                                                             { -0.515164, 1.03033, 0.694511, 4.16706, -0.0597923, 0.119585, -0.119554, 0.239108, -4.12132, -0.478338, 0., -5.55112e-17, -0.956432, -1.38778e-16 },
                                                             { -1.54549, -0.515164, -0.179377, -0.0597923, 2.08353, 0.694511, -0.358662, -0.119554, -1., 1.62977, -0.15155, -0.909108, -0.868935, 1.29983 },
                                                             { -0.515164, 1.03033, -0.0597923, 0.119585, 0.694511, 4.16706, -0.119554, 0.239108, -6.93889e-17, -0.478338, -4.12132, -5.55112e-17, -1.11022e-16, -0.956432 },
                                                             { -1.13634, -0.378779, -0.358662, -0.119554, -0.358662, -0.119554, 1.85366, 0.617887, -0.757559, -0.956432, -0.757559, 1.38778e-16, 1.23577, 1.23577 },
                                                             { -0.378779, 0.757559, -0.119554, 0.239108, -0.119554, 0.239108, 0.617887, 3.70732, -5.55112e-17, -1.11022e-16, -5.55112e-17, -3.03023, -0.956432, -0.956432 },
                                                             { 1.90911, -4.12132, -0.15155, -4.12132, -1., -6.59195e-17, -0.757559, -5.55112e-17, 15.2729, -4.6062, 0.606198, -4.44089e-16, -3.03023, 0. },
                                                             { -2.3031, -3.33067e-16, 1.62977, -0.478338, 1.62977, -0.478338, -0.956432, -1.11022e-16, -4.6062, 13.9948, -4.6062, 0., -1.91286, -1.91286 },
                                                             { 1.90911, -4.12132, -1., 0., -0.15155, -4.12132, -0.757559, -5.55112e-17, 0.606198, -4.6062, 15.2729, -4.44089e-16, 0., -3.03023 },
                                                             { 1.81822, -3.03023, -0.909108, -1.11022e-16, -0.909108, -1.11022e-16, 1.38778e-16, -3.03023, -5.55112e-16, 0., -5.55112e-16, 13.3333, -3.63643, -3.63643 },
                                                             { -1.66667, -4.44089e-16, 1.29983, -0.956432, -0.868935, -1.38778e-16, 1.23577, -0.956432, -3.03023, -1.91286, 0., -3.63643, 12.0553, -1.56288 },
                                                             { -1.66667, -4.44089e-16, -0.868935, -1.38778e-16, 1.29983, -0.956432, 1.23577, -0.956432, 0., -1.91286, -3.03023, -3.63643, -1.56288, 12.0553 },
                                                         },
                                                         expected_results,
                                                         __FILE__, __LINE__);
            }
            else
            {
                InsertNewEntry<IsMatrixOrVector::matrix>("surfacic_bidomain_operator",
                                                         content_type
                                                         {
                                                             { 4.22732, 1.40911, -1.54549, -0.515164, -1.54549, -0.515164, -1.13634, -0.378779, -1.66667, 1.90911, -2.3031, 1.90911, 1.81822, -1.66667 },
                                                             { 1.40911, 8.45465, -0.515164, 1.03033, -0.515164, 1.03033, -0.378779, 0.757559, -3.33067e-16, -4.12132, -2.22045e-16, -4.12132, -3.03023, -3.33067e-16 },
                                                             { -1.54549, -0.515164, 2.08353, 0.694511, -0.179377, -0.0597923, -0.358662, -0.119554, 1.29983, -0.15155, 1.62977, -1., -0.909108, -0.868935 },
                                                             { -0.515164, 1.03033, 0.694511, 4.16706, -0.0597923, 0.119585, -0.119554, 0.239108, -0.956432, -4.12132, -0.478338, 0., -5.55112e-17, -1.38778e-16 },
                                                             { -1.54549, -0.515164, -0.179377, -0.0597923, 2.08353, 0.694511, -0.358662, -0.119554, -0.868935, -1., 1.62977, -0.15155, -0.909108, 1.29983 },
                                                             { -0.515164, 1.03033, -0.0597923, 0.119585, 0.694511, 4.16706, -0.119554, 0.239108, -1.11022e-16, -6.93889e-17, -0.478338, -4.12132, -5.55112e-17, -0.956432 },
                                                             { -1.13634, -0.378779, -0.358662, -0.119554, -0.358662, -0.119554, 1.85366, 0.617887, 1.23577, -0.757559, -0.956432, -0.757559, 1.38778e-16, 1.23577 },
                                                             { -0.378779, 0.757559, -0.119554, 0.239108, -0.119554, 0.239108, 0.617887, 3.70732, -0.956432, -5.55112e-17, -1.11022e-16, -5.55112e-17, -3.03023, -0.956432 },
                                                             { -1.66667, -4.44089e-16, 1.29983, -0.956432, -0.868935, -1.38778e-16, 1.23577, -0.956432, 12.0553, -3.03023, -1.91286, 0., -3.63643, -1.56288 },
                                                             { 1.90911, -4.12132, -0.15155, -4.12132, -1., -6.59195e-17, -0.757559, -5.55112e-17, -3.03023, 15.2729, -4.6062, 0.606198, -4.44089e-16, 0. },
                                                             { -2.3031, -3.33067e-16, 1.62977, -0.478338, 1.62977, -0.478338, -0.956432, -1.11022e-16, -1.91286, -4.6062, 13.9948, -4.6062, 0., -1.91286 },
                                                             { 1.90911, -4.12132, -1., 0., -0.15155, -4.12132, -0.757559, -5.55112e-17, 0., 0.606198, -4.6062, 15.2729, -4.44089e-16, -3.03023 },
                                                             { 1.81822, -3.03023, -0.909108, -1.11022e-16, -0.909108, -1.11022e-16, 1.38778e-16, -3.03023, -3.63643, -5.55112e-16, 0., -5.55112e-16, 13.3333, -3.63643 },
                                                             { -1.66667, -4.44089e-16, -0.868935, -1.38778e-16, 1.29983, -0.956432, 1.23577, -0.956432, -1.56288, 0., -1.91286, -3.03023, -3.63643, 12.0553},
                                                         },
                                                         expected_results,
                                                         __FILE__, __LINE__);
            }
            
            InsertNewEntry<IsMatrixOrVector::matrix>("pk2_operator",
                                 content_type
                                 {
                                     { 2.38889, 0.388889, 0.388889, -1.05556, -0.666667, -0.666667, -0.666667, 0.277778, 0., -0.666667, 0., 0.277778 },
                                     { 0.388889, 2.38889, 0.388889, 0.277778, -0.666667, 0., -0.666667, -1.05556, -0.666667, 0., -0.666667, 0.277778 },
                                     { 0.388889, 0.388889, 2.38889, 0.277778, 0., -0.666667, 0., 0.277778, -0.666667, -0.666667, -0.666667, -1.05556 },
                                     { -1.05556, 0.277778, 0.277778, 1.05556, 0., 0., 0., -0.277778, 0., 0., 0., -0.277778 },
                                     { -0.666667, -0.666667, 0., 0., 0.666667, 0., 0.666667, 0., 0., 0., 0., 0. },
                                     { -0.666667, 0., -0.666667, 0., 0., 0.666667, 0., 0., 0., 0.666667, 0., 0. },
                                     { -0.666667, -0.666667, 0., 0., 0.666667, 0., 0.666667, 0., 0., 0., 0., 0. },
                                     { 0.277778, -1.05556, 0.277778, -0.277778, 0., 0., 0., 1.05556, 0., 0., 0., -0.277778 },
                                     { 0., -0.666667, -0.666667, 0., 0., 0., 0., 0., 0.666667, 0., 0.666667, 0. },
                                     { -0.666667, 0., -0.666667, 0., 0., 0.666667, 0., 0., 0., 0.666667, 0., 0. },
                                     { 0., -0.666667, -0.666667, 0., 0., 0., 0., 0., 0.666667, 0., 0.666667, 0. },
                                     { 0.277778, 0.277778, -1.05556, -0.277778, 0., 0., 0., -0.277778, 0., 0., 0., 1.05556 },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("scalar_div_vectorial_operator",
                                 content_type
                                 {
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -.5 },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -.5 },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -.5 },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., .5 },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., .5 },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., .5 },
                                     { -one_3rd, -one_3rd, -one_3rd, one_3rd, 0., 0., 0., one_3rd, 0., 0., 0., one_3rd, 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("stokes_operator",
                                 content_type
                                 {
                                     { .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., one_6th },
                                     { 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0., one_6th },
                                     { 0., 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, one_6th },
                                     { -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0., 0., -one_6th },
                                     { 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { 0., 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0., 0., 0. },
                                     { 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0., -one_6th },
                                     { 0., 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0. },
                                     { -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0., 0., 0. },
                                     { 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0., 0. },
                                     { 0., 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, -one_6th },
                                     { one_6th, one_6th, one_6th, -one_6th, 0., 0., 0., -one_6th, 0., 0., 0., -one_6th, 0. },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("elasticity_operator",
                                 content_type
                                 {
                                     { two_3rd, one_6th, one_6th, -one_3rd, -one_6th, -one_6th, -one_6th, 0., 0., -one_6th, 0., 0. },
                                     { one_6th, two_3rd, one_6th, 0., -one_6th, 0., -one_6th, -one_3rd, -one_6th, 0., -one_6th, 0. },
                                     { one_6th, one_6th, two_3rd, 0., 0., -one_6th, 0., 0., -one_6th, -one_6th, -one_6th, -one_3rd },
                                     { -one_3rd, 0., 0., one_3rd, 0., 0., 0., 0., 0., 0., 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, 0., one_6th, 0., 0., 0., 0., 0. },
                                     { -one_6th, 0., -one_6th, 0., 0., one_6th, 0., 0., 0., one_6th, 0., 0. },
                                     { -one_6th, -one_6th, 0., 0., one_6th, 0., one_6th, 0., 0., 0., 0., 0. },
                                     { 0., -one_3rd, 0., 0., 0., 0., 0., one_3rd, 0., 0., 0., 0. },
                                     { 0., -one_6th, -one_6th, 0., 0., 0., 0., 0., one_6th, 0., one_6th, 0. },
                                     { -one_6th, 0., -one_6th, 0., 0., one_6th, 0., 0., 0., one_6th, 0., 0. },
                                     { 0., -one_6th, -one_6th, 0., 0., 0., 0., 0., one_6th, 0., one_6th, 0. },
                                     { 0., 0., -one_3rd, 0., 0., 0., 0., 0., 0., 0., 0., one_3rd },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            constexpr double one_24th = 1. / 24.;
            
            InsertNewEntry<IsMatrixOrVector::matrix>("ale_operator_1",
                                 content_type
                                 {
                                     { -.125, 0., 0., -.125, 0., 0., -.125, 0., 0., -.125, 0., 0. },
                                     { 0., -.125, 0., 0., -.125, 0., 0., -.125, 0., 0., -.125, 0. },
                                     { 0., 0., -.125, 0., 0., -.125, 0., 0., -.125, 0., 0., -.125 },
                                     { one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0. },
                                     { 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0. },
                                     { 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th },
                                     { one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0. },
                                     { 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0. },
                                     { 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th },
                                     { one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0. },
                                     { 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0. },
                                     { 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th, 0., 0., one_24th },
                                 },
                                 expected_results,
                                 __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("ale_operator_2",
                                                     content_type
                                                     {
                                                         { -3. * .05, 0., 0., -.1, 0., 0., -one_12th, 0., 0., -one_12th, 0., 0. },
                                                         { 0., -3. * .05, 0., 0., -.1, 0., 0., -one_12th, 0., 0., -one_12th, 0. },
                                                         { 0., 0., -3. * .05, 0., 0., -.1, 0., 0., -one_12th, 0., 0., -one_12th },
                                                         { one_120th, 0., 0., -one_120th, 0., 0., 0., 0., 0., 0., 0., 0. },
                                                         { 0., one_120th, 0., 0., -one_120th, 0., 0., 0., 0., 0., 0., 0. },
                                                         { 0., 0., one_120th, 0., 0., -one_120th, 0., 0., 0., 0., 0., 0. },
                                                         { one_120th, 0., 0., -one_120th, 0., 0., -one_30th, 0., 0., -one_120th, 0., 0. },
                                                         { 0., one_120th, 0., 0., -one_120th, 0., 0., -one_30th, 0., 0., -one_120th, 0. },
                                                         { 0., 0., one_120th, 0., 0., -one_120th, 0., 0., -one_30th, 0., 0., -one_120th },
                                                         { one_120th, 0., 0., -one_120th, 0., 0., -one_120th, 0., 0., -one_30th, 0., 0. },
                                                         { 0., one_120th, 0., 0., -one_120th, 0., 0., -one_120th, 0., 0., -one_30th, 0. },
                                                         { 0., 0., one_120th, 0., 0., -one_120th, 0., 0., -one_120th, 0., 0., -one_30th },
                                                     },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::matrix>("following_pressure_operator",
                                                     content_type
                                                     {
                                                         { 0., -one_3rd, 0., 0., one_6th, -one_6th, 0., one_6th, 0., 0., 0., one_6th },
                                                         { one_3rd, 0., one_3rd, -one_6th, 0., 0., -one_6th, 0., -one_6th, 0., 0., -one_6th },
                                                         { 0., -one_3rd, 0., one_6th, 0., 0., 0., one_6th, 0., -one_6th, one_6th, 0. },
                                                         { 0., -one_6th, one_6th, 0., 0., -one_3rd, 0., one_6th, 0., 0., 0., one_6th },
                                                         { one_6th, 0., one_3rd, 0., 0., 0., -one_6th, 0., -one_3rd, 0., 0., 0. },
                                                         { -one_6th, -one_3rd, 0., one_3rd, 0., 0., 0., one_3rd, 0., -one_6th, 0., 0. },
                                                         { 0., -one_6th, 0., 0., -one_6th, -one_3rd, 0., one_3rd, 0., 0., 0., one_3rd },
                                                         { one_6th, 0., one_6th, one_6th, 0., 0., -one_3rd, 0., -one_3rd, 0., 0., one_6th },
                                                         { 0., -one_6th, 0., one_3rd, 0., 0., 0., one_3rd, 0., -one_3rd, -one_6th, 0. },
                                                         { 0., -one_3rd, -one_6th, 0., 0., -one_6th, 0., one_3rd, 0., 0., 0., one_3rd },
                                                         { one_3rd, 0., one_6th, 0., 0., 0., -one_3rd, 0., -one_6th, 0., 0., 0. },
                                                         { one_6th, -one_6th, 0., one_6th, 0., 0., 0., one_6th, 0., -one_3rd, 0., 0. },
                                                     },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            return expected_results;
        }
        
        
        expected_results_type<IsMatrixOrVector::vector> GetExpectedVectorialResults3D(bool is_parallel)
        {
            static_cast<void>(is_parallel); // For 3D case vectors are identical in both sequential and
                                            // parallel case.
            
            expected_results_type<IsMatrixOrVector::vector> expected_results;
            
            constexpr double one_24th = 1. / 24.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1",
                                                     { one_24th, one_24th, one_24th, one_24th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
         
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1_potential_1",
                                                     { one_24th, 0., one_24th, 0., one_24th, 0., one_24th, 0. },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_1_potential_2",
                                                     { one_24th, one_24th, one_24th, one_24th, one_24th, one_24th, one_24th, one_24th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            InsertNewEntry<IsMatrixOrVector::vector>("pk2_operator",
                                                     { 1., 2., 2., 2., 0., 0., 0., 0., 0., 0., 0., 0. },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            constexpr double one_120th = 1. / 120.;
            constexpr double one_30th = 1. / 30.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("source_operator_potential_3",
                                                     { -one_120th, -one_120th, -one_120th, -one_120th,
                                                         one_30th, one_30th, one_30th, one_30th, one_30th, one_30th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            constexpr double one_144th = 1. / 144.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("non_linear_source_operator",
                                                     { -one_144th, -one_144th, -one_144th, -one_144th },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            constexpr double one_6th = 1. / 6.;
            constexpr double one_3rd = 1. / 3.;
            
            InsertNewEntry<IsMatrixOrVector::vector>("following_pressure_operator",
                                                     {
                                                         one_6th, -one_6th, one_6th, one_6th,
                                                         0., one_3rd, one_3rd, one_6th,
                                                         one_3rd, one_3rd, 0., one_6th
                                                     },
                                                     expected_results,
                                                     __FILE__, __LINE__);
            
            return expected_results;
        }
        
     
    } // namespace TestFunctionsNS


} // namespace MoReFEM


