/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 12:27:37 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Core/InputParameter/Result.hpp"

#include "Test/Operators/NonConformInterpolator/FromVertexMatching/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestVertexMatchingNS
    {
        
        
        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data),
        output_directory_(morefem_data.GetResultDirectory())
        { }


        void Model::SupplInitialize(const morefem_data_type& morefem_data)
        {
            using type = NonConformInterpolatorNS::FromVertexMatching;
            
            unknown_solid_2_fluid_ =
                std::make_unique<type>(morefem_data.GetInputParameterList(),
                                       EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_solid),
                                       EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_fluid),
                                       NonConformInterpolatorNS::store_matrix_pattern::yes);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }


    } // namespace TestVertexMatchingNS


} // namespace MoReFEM
